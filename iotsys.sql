/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE `config_table` (
	`key`	TEXT UNIQUE,
	`value`	TEXT,
	`value_default`	TEXT,
	`meta_id`	INTEGER,
	`read_only`	INTEGER,
	`internal_only`	INTEGER,
	FOREIGN KEY(`meta_id`) REFERENCES `meta_table`(`id`)
);
INSERT INTO "config_table" VALUES('iotsys.settings.system.devicefriendlyname','Vipertooth 0.1','Vipertooth 0.1',1,0,0);
CREATE TABLE `meta_table` (
	`id`	INTEGER,
	`description`	TEXT,
	`value_type`	TEXT,
	`value_constraints`	TEXT,
	PRIMARY KEY(`id`)
);
INSERT INTO "meta_table" VALUES(1,'Default constraints for UKNOWN types','UKNOWN',NULL);
CREATE TABLE `database_meta_table` (
	`key`	TEXT,
	`value`	TEXT
);
INSERT INTO "database_meta_table" VALUES('database_version','1.0');
COMMIT;
