# iotsys

### About:
- IoTSys process is used for mobile phone to Vipertooth communication for system related settings like VoiceUI, Bluetooth and Wifi settings.
- It provides an OCF(Open Connectivity Foundation) based protocol for device to device connectivity. For example, to request the vipertooth board to enable a VoiceUI client, get battery status, get WiFi IP address, start Bluetooth scan etc.
- These Get and Post requests are sent from mobile phone to Vipertooth through an IoTivity connection and IoTSys is responsible to forward them to the corresponding software modules in their acceptable format.

### Dependencies
- Depends on the following
  - IoTivity package
  - Connectivity manager for bluetooth and network settings
  - Voice UI framework
  - System module
