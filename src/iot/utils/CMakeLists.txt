target_sources(${PROJECT_LIBNAME} PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/stack.h
    ${CMAKE_CURRENT_SOURCE_DIR}/stack.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/helper.h
    ${CMAKE_CURRENT_SOURCE_DIR}/helper.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/config-keys.h
)
