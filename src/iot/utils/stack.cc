/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "iot/utils/stack.h"

#include <OCPlatform.h>

#include "adk/log.h"

namespace adk {
namespace iotivity_iotsys {

IoTivityStack::IoTivityStack(ModeType mode)
    : status_{OC_STACK_ERROR} {
    ADK_LOG_INFO("IoTivityStack: Initializing stack");
    OC::PlatformConfig cfg{OC::ServiceType::InProc, mode,
        static_cast<OCConnectivityType>(CT_IP_USE_V4),
        static_cast<OCConnectivityType>(CT_IP_USE_V4),
        static_cast<OCTransportAdapter>(OCTransportAdapter::OC_ADAPTER_IP),
        OC::QualityOfService::HighQos};

    OC::OCPlatform::Configure(cfg);
    ADK_LOG_INFO("IoTivityStack: Initializing stack");
}

IoTivityStack::~IoTivityStack() {
    if (status_ == OC_STACK_OK) {
        OC::OCPlatform::stop();
    }
}

OCStackResult IoTivityStack::start() {
    status_ = OC::OCPlatform::start();
    return status_;
}

}  // namespace iotivity_iotsys
}  // namespace adk
