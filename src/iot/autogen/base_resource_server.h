/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_IOT_AUTOGEN_BASE_RESOURCE_SERVER_H_
#define SRC_IOT_AUTOGEN_BASE_RESOURCE_SERVER_H_

#include <OCApi.h>
#include <memory>
#include <mutex>
#include <queue>
#include <string>
#include <tuple>
#include <vector>

namespace adk {
namespace iotivity_iotsys {

using OC::ObservationIds;
using OC::OCRepresentation;
using OC::OCResourceRequest;
using OC::QueryParamsMap;

using GetCb = std::function<std::tuple<OCEntityHandlerResult,
    OCRepresentation>(const QueryParamsMap&)>;
using PostCb = std::function<std::tuple<OCEntityHandlerResult,
    OCRepresentation>(const QueryParamsMap&, const OCRepresentation&)>;

class BaseResourceServer {
 public:
    BaseResourceServer(GetCb get_callback, PostCb post_callback)
        : get_cb_{std::move(get_callback)}, post_cb_{std::move(post_callback)} {}
    explicit BaseResourceServer(GetCb get_callback)
        : get_cb_{std::move(get_callback)} {}
    explicit BaseResourceServer(PostCb post_callback)
        : post_cb_{std::move(post_callback)} {}
    BaseResourceServer(BaseResourceServer const&) = delete;
    BaseResourceServer(BaseResourceServer&&) = delete;
    BaseResourceServer& operator=(BaseResourceServer const&) = delete;
    BaseResourceServer& operator=(BaseResourceServer&&) = delete;
    ~BaseResourceServer();

    bool registerResource(std::string res_uri,
        const std::string& res_type, const std::string& res_iface,
        std::vector<std::string> extra_ifaces, uint8_t res_prop);
    void addNotifyQueue(OCRepresentation rep);

 private:
    OCEntityHandlerResult requestHandler(const std::shared_ptr<OCResourceRequest>& request);
    OCEntityHandlerResult observeHandler(const std::shared_ptr<OCResourceRequest>& request);

    void notify();

 private:
    OCResourceHandle resource_handle_;
    ObservationIds interested_observers_;

    std::mutex base_resource_server_mutex_;
    std::mutex notif_mutex_;
    std::queue<OCRepresentation> notif_rep_queue_;

    bool request_running_ = false;
    GetCb get_cb_;
    PostCb post_cb_;
};
}  // namespace iotivity_iotsys
}  // namespace adk

#endif  // SRC_IOT_AUTOGEN_BASE_RESOURCE_SERVER_H_
