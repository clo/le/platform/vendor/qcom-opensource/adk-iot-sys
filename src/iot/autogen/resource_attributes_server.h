/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_IOT_AUTOGEN_RESOURCE_ATTRIBUTES_SERVER_H_
#define SRC_IOT_AUTOGEN_RESOURCE_ATTRIBUTES_SERVER_H_

#include <string>
#include <unordered_set>
#include <vector>

#include <OCRepresentation.h>

#include "iot/autogen/resource_attr_strings.h"

namespace adk {
namespace iotivity_iotsys {

using OC::OCRepresentation;

class AVSOnboardingErrorServerAttr {
 public:
    enum class Error {
        kTimedout,
        kUnknown,
    };

    std::string getClient() const {
        return client_;
    }

    Error getError() const {
        return error_;
    }

    int getReattempt() const {
        return reattempt_;
    }

    void setClient(std::string client) {
        client_ = std::move(client);
        packed_attributes_.emplace(kClient);
    }

    void setError(Error error) {
        error_ = error;
        packed_attributes_.emplace(kError);
    }

    void setReattempt(int reattempt) {
        reattempt_ = reattempt;
        packed_attributes_.emplace(kReattempt);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

    Error errorFromString(const std::string& str) const;
    std::string errorToString(Error val) const;

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    std::string client_;
    Error error_;
    int reattempt_;
};

class AccessPointServerAttr {
 public:
    std::string getRSSI() const {
        return rssi_;
    }

    std::string getSSID() const {
        return ssid_;
    }

    void setRSSI(std::string rssi) {
        rssi_ = std::move(rssi);
        packed_attributes_.emplace(kRSSI);
    }

    void setSSID(std::string ssid) {
        ssid_ = std::move(ssid);
        packed_attributes_.emplace(kSSID);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    std::string rssi_;
    std::string ssid_;
};

class AddressInServerAttr {
 public:
    std::string getAddress() const {
        return address_;
    }

    void setAddress(std::string address) {
        address_ = std::move(address);
        packed_attributes_.emplace(kAddress);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    std::string address_;
};

class AuthenticateAVSServerAttr {
 public:
    std::string getCode() const {
        return code_;
    }

    std::string getUrl() const {
        return url_;
    }

    void setCode(std::string code) {
        code_ = std::move(code);
        packed_attributes_.emplace(kCode);
    }

    void setUrl(std::string url) {
        url_ = std::move(url);
        packed_attributes_.emplace(kUrl);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    std::string code_;
    std::string url_;
};

class BatteryStatusServerAttr {
 public:
    bool getBatteryPowered() const {
        return battery_powered_;
    }

    int getChargeLevel() const {
        return charge_level_;
    }

    int getTimeTillDischarge() const {
        return time_till_discharge_;
    }

    int getTimeTillFullCharge() const {
        return time_till_full_charge_;
    }

    void setBatteryPowered(bool battery_powered) {
        battery_powered_ = battery_powered;
        packed_attributes_.emplace(kBatteryPowered);
    }

    void setChargeLevel(int charge_level) {
        charge_level_ = charge_level;
        packed_attributes_.emplace(kChargeLevel);
    }

    void setTimeTillDischarge(int time_till_discharge) {
        time_till_discharge_ = time_till_discharge;
        packed_attributes_.emplace(kTimeTillDischarge);
    }

    void setTimeTillFullCharge(int time_till_full_charge) {
        time_till_full_charge_ = time_till_full_charge;
        packed_attributes_.emplace(kTimeTillFullCharge);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    bool battery_powered_;
    int charge_level_;
    int time_till_discharge_;
    int time_till_full_charge_;
};

class BtDeviceStatusServerAttr {
 public:
    std::string getAddress() const {
        return address_;
    }

    std::string getName() const {
        return name_;
    }

    bool getStatus() const {
        return status_;
    }

    void setAddress(std::string address) {
        address_ = std::move(address);
        packed_attributes_.emplace(kAddress);
    }

    void setName(std::string name) {
        name_ = std::move(name);
        packed_attributes_.emplace(kName);
    }

    void setStatus(bool status) {
        status_ = status;
        packed_attributes_.emplace(kStatus);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    std::string address_;
    std::string name_;
    bool status_;
};

class BtErrorServerAttr {
 public:
    std::string getAddress() const {
        return address_;
    }

    std::string getStatus() const {
        return status_;
    }

    void setAddress(std::string address) {
        address_ = std::move(address);
        packed_attributes_.emplace(kAddress);
    }

    void setStatus(std::string status) {
        status_ = std::move(status);
        packed_attributes_.emplace(kStatus);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    std::string address_;
    std::string status_;
};

class BtDeviceServerAttr {
 public:
    std::string getAddress() const {
        return address_;
    }

    std::string getName() const {
        return name_;
    }

    void setAddress(std::string address) {
        address_ = std::move(address);
        packed_attributes_.emplace(kAddress);
    }

    void setName(std::string name) {
        name_ = std::move(name);
        packed_attributes_.emplace(kName);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    std::string address_;
    std::string name_;
};

class BtSignalServerAttr {
 public:
    enum class Name {
        kScanStopped,
        kPairedListCleared,
        kUnknown,
    };

    Name getName() const {
        return name_;
    }

    std::string getStatus() const {
        return status_;
    }

    void setName(Name name) {
        name_ = name;
        packed_attributes_.emplace(kName);
    }

    void setStatus(std::string status) {
        status_ = std::move(status);
        packed_attributes_.emplace(kStatus);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

    Name nameFromString(const std::string& str) const;
    std::string nameToString(Name val) const;

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    Name name_;
    std::string status_;
};

class BluetoothServerAttr {
 public:
    enum class AdapterState {
        kEnabled,
        kDisabled,
        kUnknown,
    };

    enum class Discoverable {
        kDiscoverable,
        kNonDiscoverable,
        kUnknown,
    };

    AdapterState getAdapterState() const {
        return adapter_state_;
    }

    BtDeviceStatusServerAttr getConnectedState() const {
        return connected_state_;
    }

    Discoverable getDiscoverable() const {
        return discoverable_;
    }

    BtErrorServerAttr getError() const {
        return error_;
    }

    std::vector<BtDeviceStatusServerAttr> getPairedDevices() const {
        return paired_devices_;
    }

    BtDeviceStatusServerAttr getPairedState() const {
        return paired_state_;
    }

    BtDeviceServerAttr getScanResult() const {
        return scan_result_;
    }

    BtSignalServerAttr getSignal() const {
        return signal_;
    }

    void setAdapterState(AdapterState adapter_state) {
        adapter_state_ = adapter_state;
        packed_attributes_.emplace(kAdapterState);
    }

    void setConnectedState(BtDeviceStatusServerAttr connected_state) {
        connected_state_ = std::move(connected_state);
        packed_attributes_.emplace(kConnectedState);
    }

    void setDiscoverable(Discoverable discoverable) {
        discoverable_ = discoverable;
        packed_attributes_.emplace(kDiscoverable);
    }

    void setError(BtErrorServerAttr error) {
        error_ = std::move(error);
        packed_attributes_.emplace(kError);
    }

    void setPairedDevices(std::vector<BtDeviceStatusServerAttr> paired_devices) {
        paired_devices_ = std::move(paired_devices);
        packed_attributes_.emplace(kPairedDevices);
    }

    void setPairedState(BtDeviceStatusServerAttr paired_state) {
        paired_state_ = std::move(paired_state);
        packed_attributes_.emplace(kPairedState);
    }

    void setScanResult(BtDeviceServerAttr scan_result) {
        scan_result_ = std::move(scan_result);
        packed_attributes_.emplace(kScanResult);
    }

    void setSignal(BtSignalServerAttr signal) {
        signal_ = std::move(signal);
        packed_attributes_.emplace(kSignal);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

    AdapterState adapterStateFromString(const std::string& str) const;
    std::string adapterStateToString(AdapterState val) const;
    Discoverable discoverableFromString(const std::string& str) const;
    std::string discoverableToString(Discoverable val) const;

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    AdapterState adapter_state_;
    BtDeviceStatusServerAttr connected_state_;
    Discoverable discoverable_;
    BtErrorServerAttr error_;
    std::vector<BtDeviceStatusServerAttr> paired_devices_;
    BtDeviceStatusServerAttr paired_state_;
    BtDeviceServerAttr scan_result_;
    BtSignalServerAttr signal_;
};

class ClientInServerAttr {
 public:
    std::string getClient() const {
        return client_;
    }

    void setClient(std::string client) {
        client_ = std::move(client);
        packed_attributes_.emplace(kClient);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    std::string client_;
};

class EnableBtAdapterInServerAttr {
 public:
    bool getAdapterState() const {
        return adapter_state_;
    }

    void setAdapterState(bool adapter_state) {
        adapter_state_ = adapter_state;
        packed_attributes_.emplace(kAdapterState);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    bool adapter_state_;
};

class EnableZbAdapterInServerAttr {
 public:
    bool getAdapterState() const {
        return adapter_state_;
    }

    void setAdapterState(bool adapter_state) {
        adapter_state_ = adapter_state;
        packed_attributes_.emplace(kAdapterState);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    bool adapter_state_;
};

class NetworkInterfaceServerAttr {
 public:
    std::string getIPAddress() const {
        return ip_address_;
    }

    bool getConnectedState() const {
        return connected_state_;
    }

    std::string getMacAddress() const {
        return mac_address_;
    }

    void setIPAddress(std::string ip_address) {
        ip_address_ = std::move(ip_address);
        packed_attributes_.emplace(kIPAddress);
    }

    void setConnectedState(bool connected_state) {
        connected_state_ = connected_state;
        packed_attributes_.emplace(kConnectedState);
    }

    void setMacAddress(std::string mac_address) {
        mac_address_ = std::move(mac_address);
        packed_attributes_.emplace(kMacAddress);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    std::string ip_address_;
    bool connected_state_;
    std::string mac_address_;
};

class NetworkServerAttr {
 public:
    AccessPointServerAttr getAccessPoint() const {
        return access_point_;
    }

    NetworkInterfaceServerAttr getEthernetAdapter() const {
        return ethernet_adapter_;
    }

    NetworkInterfaceServerAttr getWifiAdapter() const {
        return wifi_adapter_;
    }

    void setAccessPoint(AccessPointServerAttr access_point) {
        access_point_ = std::move(access_point);
        packed_attributes_.emplace(kAccessPoint);
    }

    void setEthernetAdapter(NetworkInterfaceServerAttr ethernet_adapter) {
        ethernet_adapter_ = std::move(ethernet_adapter);
        packed_attributes_.emplace(kEthernetAdapter);
    }

    void setWifiAdapter(NetworkInterfaceServerAttr wifi_adapter) {
        wifi_adapter_ = std::move(wifi_adapter);
        packed_attributes_.emplace(kWifiAdapter);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    AccessPointServerAttr access_point_;
    NetworkInterfaceServerAttr ethernet_adapter_;
    NetworkInterfaceServerAttr wifi_adapter_;
};

class SelectClientInServerAttr {
 public:
    std::string getClient() const {
        return client_;
    }

    bool getWakewordStatus() const {
        return wakeword_status_;
    }

    void setClient(std::string client) {
        client_ = std::move(client);
        packed_attributes_.emplace(kClient);
    }

    void setWakewordStatus(bool wakeword_status) {
        wakeword_status_ = wakeword_status;
        packed_attributes_.emplace(kWakewordStatus);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    std::string client_;
    bool wakeword_status_;
};

class SetZbFriendlyNameInServerAttr {
 public:
    int getDeviceIdentifier() const {
        return device_identifier_;
    }

    std::string getFriendlyName() const {
        return friendly_name_;
    }

    void setDeviceIdentifier(int device_identifier) {
        device_identifier_ = device_identifier;
        packed_attributes_.emplace(kDeviceIdentifier);
    }

    void setFriendlyName(std::string friendly_name) {
        friendly_name_ = std::move(friendly_name);
        packed_attributes_.emplace(kFriendlyName);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    int device_identifier_;
    std::string friendly_name_;
};

class SystemServerAttr {
 public:
    BatteryStatusServerAttr getBatteryStatus() const {
        return battery_status_;
    }

    bool getBatterySupported() const {
        return battery_supported_;
    }

    std::string getDeviceFriendlyName() const {
        return device_friendly_name_;
    }

    std::string getFirmwareVersion() const {
        return firmware_version_;
    }

    std::string getManufacturer() const {
        return manufacturer_;
    }

    std::string getModel() const {
        return model_;
    }

    void setBatteryStatus(BatteryStatusServerAttr battery_status) {
        battery_status_ = std::move(battery_status);
        packed_attributes_.emplace(kBatteryStatus);
    }

    void setBatterySupported(bool battery_supported) {
        battery_supported_ = battery_supported;
        packed_attributes_.emplace(kBatterySupported);
    }

    void setDeviceFriendlyName(std::string device_friendly_name) {
        device_friendly_name_ = std::move(device_friendly_name);
        packed_attributes_.emplace(kDeviceFriendlyName);
    }

    void setFirmwareVersion(std::string firmware_version) {
        firmware_version_ = std::move(firmware_version);
        packed_attributes_.emplace(kFirmwareVersion);
    }

    void setManufacturer(std::string manufacturer) {
        manufacturer_ = std::move(manufacturer);
        packed_attributes_.emplace(kManufacturer);
    }

    void setModel(std::string model) {
        model_ = std::move(model);
        packed_attributes_.emplace(kModel);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    BatteryStatusServerAttr battery_status_;
    bool battery_supported_;
    std::string device_friendly_name_;
    std::string firmware_version_;
    std::string manufacturer_;
    std::string model_;
};

class TimeoutSecInServerAttr {
 public:
    int getTimeoutSecs() const {
        return timeout_secs_;
    }

    void setTimeoutSecs(int timeout_secs) {
        timeout_secs_ = timeout_secs;
        packed_attributes_.emplace(kTimeoutSecs);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    int timeout_secs_;
};

class VoiceUIClientServerAttr {
 public:
    std::string getName() const {
        return name_;
    }

    std::string getVersion() const {
        return version_;
    }

    bool getWakewordStatus() const {
        return wakeword_status_;
    }

    void setName(std::string name) {
        name_ = std::move(name);
        packed_attributes_.emplace(kName);
    }

    void setVersion(std::string version) {
        version_ = std::move(version);
        packed_attributes_.emplace(kVersion);
    }

    void setWakewordStatus(bool wakeword_status) {
        wakeword_status_ = wakeword_status;
        packed_attributes_.emplace(kWakewordStatus);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    std::string name_;
    std::string version_;
    bool wakeword_status_;
};

class VoiceUIServerAttr {
 public:
    std::vector<std::string> getASRProviders() const {
        return asr_providers_;
    }

    std::string getAVSCredentialFile() const {
        return avs_credential_file_;
    }

    std::string getAVSLocale() const {
        return avs_locale_;
    }

    std::vector<std::string> getAVSLocaleList() const {
        return avs_locale_list_;
    }

    bool getAVSOnboarded() const {
        return avs_onboarded_;
    }

    AVSOnboardingErrorServerAttr getAVSOnboardingError() const {
        return avs_onboarding_error_;
    }

    std::vector<std::string> getNLUProviders() const {
        return nlu_providers_;
    }

    std::vector<std::string> getTTSProviders() const {
        return tts_providers_;
    }

    AuthenticateAVSServerAttr getAuthenticateAVS() const {
        return authenticate_avs_;
    }

    std::string getDefaultVoiceUIClient() const {
        return default_voice_ui_client_;
    }

    bool getEnableVoiceUI() const {
        return enable_voice_ui_;
    }

    std::vector<std::string> getModularClientLanguages() const {
        return modular_client_languages_;
    }

    std::string getSelectedASRProvider() const {
        return selected_asr_provider_;
    }

    std::string getSelectedModularClientLanguage() const {
        return selected_modular_client_language_;
    }

    std::string getSelectedNLUProvider() const {
        return selected_nlu_provider_;
    }

    std::string getSelectedTTSProvider() const {
        return selected_tts_provider_;
    }

    std::vector<VoiceUIClientServerAttr> getVoiceUIClients() const {
        return voice_ui_clients_;
    }

    void setASRProviders(std::vector<std::string> asr_providers) {
        asr_providers_ = std::move(asr_providers);
        packed_attributes_.emplace(kASRProviders);
    }

    void setAVSCredentialFile(std::string avs_credential_file) {
        avs_credential_file_ = std::move(avs_credential_file);
        packed_attributes_.emplace(kAVSCredentialFile);
    }

    void setAVSLocale(std::string avs_locale) {
        avs_locale_ = std::move(avs_locale);
        packed_attributes_.emplace(kAVSLocale);
    }

    void setAVSLocaleList(std::vector<std::string> avs_locale_list) {
        avs_locale_list_ = std::move(avs_locale_list);
        packed_attributes_.emplace(kAVSLocaleList);
    }

    void setAVSOnboarded(bool avs_onboarded) {
        avs_onboarded_ = avs_onboarded;
        packed_attributes_.emplace(kAVSOnboarded);
    }

    void setAVSOnboardingError(AVSOnboardingErrorServerAttr avs_onboarding_error) {
        avs_onboarding_error_ = std::move(avs_onboarding_error);
        packed_attributes_.emplace(kAVSOnboardingError);
    }

    void setNLUProviders(std::vector<std::string> nlu_providers) {
        nlu_providers_ = std::move(nlu_providers);
        packed_attributes_.emplace(kNLUProviders);
    }

    void setTTSProviders(std::vector<std::string> tts_providers) {
        tts_providers_ = std::move(tts_providers);
        packed_attributes_.emplace(kTTSProviders);
    }

    void setAuthenticateAVS(AuthenticateAVSServerAttr authenticate_avs) {
        authenticate_avs_ = std::move(authenticate_avs);
        packed_attributes_.emplace(kAuthenticateAVS);
    }

    void setDefaultVoiceUIClient(std::string default_voice_ui_client) {
        default_voice_ui_client_ = std::move(default_voice_ui_client);
        packed_attributes_.emplace(kDefaultVoiceUIClient);
    }

    void setEnableVoiceUI(bool enable_voice_ui) {
        enable_voice_ui_ = enable_voice_ui;
        packed_attributes_.emplace(kEnableVoiceUI);
    }

    void setModularClientLanguages(std::vector<std::string> modular_client_languages) {
        modular_client_languages_ = std::move(modular_client_languages);
        packed_attributes_.emplace(kModularClientLanguages);
    }

    void setSelectedASRProvider(std::string selected_asr_provider) {
        selected_asr_provider_ = std::move(selected_asr_provider);
        packed_attributes_.emplace(kSelectedASRProvider);
    }

    void setSelectedModularClientLanguage(std::string selected_modular_client_language) {
        selected_modular_client_language_ = std::move(selected_modular_client_language);
        packed_attributes_.emplace(kSelectedModularClientLanguage);
    }

    void setSelectedNLUProvider(std::string selected_nlu_provider) {
        selected_nlu_provider_ = std::move(selected_nlu_provider);
        packed_attributes_.emplace(kSelectedNLUProvider);
    }

    void setSelectedTTSProvider(std::string selected_tts_provider) {
        selected_tts_provider_ = std::move(selected_tts_provider);
        packed_attributes_.emplace(kSelectedTTSProvider);
    }

    void setVoiceUIClients(std::vector<VoiceUIClientServerAttr> voice_ui_clients) {
        voice_ui_clients_ = std::move(voice_ui_clients);
        packed_attributes_.emplace(kVoiceUIClients);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    std::vector<std::string> asr_providers_;
    std::string avs_credential_file_;
    std::string avs_locale_;
    std::vector<std::string> avs_locale_list_;
    bool avs_onboarded_;
    AVSOnboardingErrorServerAttr avs_onboarding_error_;
    std::vector<std::string> nlu_providers_;
    std::vector<std::string> tts_providers_;
    AuthenticateAVSServerAttr authenticate_avs_;
    std::string default_voice_ui_client_;
    bool enable_voice_ui_;
    std::vector<std::string> modular_client_languages_;
    std::string selected_asr_provider_;
    std::string selected_modular_client_language_;
    std::string selected_nlu_provider_;
    std::string selected_tts_provider_;
    std::vector<VoiceUIClientServerAttr> voice_ui_clients_;
};

class ZbDeviceServerAttr {
 public:
    int getDeviceIdentifier() const {
        return device_identifier_;
    }

    std::string getDeviceType() const {
        return device_type_;
    }

    std::string getFriendlyName() const {
        return friendly_name_;
    }

    void setDeviceIdentifier(int device_identifier) {
        device_identifier_ = device_identifier;
        packed_attributes_.emplace(kDeviceIdentifier);
    }

    void setDeviceType(std::string device_type) {
        device_type_ = std::move(device_type);
        packed_attributes_.emplace(kDeviceType);
    }

    void setFriendlyName(std::string friendly_name) {
        friendly_name_ = std::move(friendly_name);
        packed_attributes_.emplace(kFriendlyName);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    int device_identifier_;
    std::string device_type_;
    std::string friendly_name_;
};

class ZbSignalServerAttr {
 public:
    enum class SignalName {
        kNetworkFormed,
        kNetworkJoined,
        kUnknown,
    };

    SignalName getSignalName() const {
        return signal_name_;
    }

    bool getStatus() const {
        return status_;
    }

    void setSignalName(SignalName signal_name) {
        signal_name_ = signal_name;
        packed_attributes_.emplace(kSignalName);
    }

    void setStatus(bool status) {
        status_ = status;
        packed_attributes_.emplace(kStatus);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

    SignalName signalNameFromString(const std::string& str) const;
    std::string signalNameToString(SignalName val) const;

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    SignalName signal_name_;
    bool status_;
};

class ZigbeeServerAttr {
 public:
    enum class AdapterState {
        kEnabled,
        kDisabled,
        kUnknown,
    };

    enum class CoordinatorState {
        kNominated,
        kNotNominated,
        kUnknown,
    };

    enum class JoiningState {
        kAllowed,
        kDisallowed,
        kUnknown,
    };

    AdapterState getAdapterState() const {
        return adapter_state_;
    }

    CoordinatorState getCoordinatorState() const {
        return coordinator_state_;
    }

    std::string getError() const {
        return error_;
    }

    std::vector<ZbDeviceServerAttr> getJoinedDevices() const {
        return joined_devices_;
    }

    JoiningState getJoiningState() const {
        return joining_state_;
    }

    ZbSignalServerAttr getSignal() const {
        return signal_;
    }

    void setAdapterState(AdapterState adapter_state) {
        adapter_state_ = adapter_state;
        packed_attributes_.emplace(kAdapterState);
    }

    void setCoordinatorState(CoordinatorState coordinator_state) {
        coordinator_state_ = coordinator_state;
        packed_attributes_.emplace(kCoordinatorState);
    }

    void setError(std::string error) {
        error_ = std::move(error);
        packed_attributes_.emplace(kError);
    }

    void setJoinedDevices(std::vector<ZbDeviceServerAttr> joined_devices) {
        joined_devices_ = std::move(joined_devices);
        packed_attributes_.emplace(kJoinedDevices);
    }

    void setJoiningState(JoiningState joining_state) {
        joining_state_ = joining_state;
        packed_attributes_.emplace(kJoiningState);
    }

    void setSignal(ZbSignalServerAttr signal) {
        signal_ = std::move(signal);
        packed_attributes_.emplace(kSignal);
    }

    std::unordered_set<std::string> unpackedAttributes() {
        return unpacked_attributes_;
    }

    OCRepresentation pack() const;
    void unpack(const OCRepresentation& rep);

    AdapterState adapterStateFromString(const std::string& str) const;
    std::string adapterStateToString(AdapterState val) const;
    CoordinatorState coordinatorStateFromString(const std::string& str) const;
    std::string coordinatorStateToString(CoordinatorState val) const;
    JoiningState joiningStateFromString(const std::string& str) const;
    std::string joiningStateToString(JoiningState val) const;

 private:
    std::unordered_set<std::string> packed_attributes_;
    std::unordered_set<std::string> unpacked_attributes_;

    AdapterState adapter_state_;
    CoordinatorState coordinator_state_;
    std::string error_;
    std::vector<ZbDeviceServerAttr> joined_devices_;
    JoiningState joining_state_;
    ZbSignalServerAttr signal_;
};

}  // namespace iotivity_iotsys
}  // namespace adk
#endif  // SRC_IOT_AUTOGEN_RESOURCE_ATTRIBUTES_SERVER_H_
