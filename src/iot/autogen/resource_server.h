/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_IOT_AUTOGEN_RESOURCE_SERVER_H_
#define SRC_IOT_AUTOGEN_RESOURCE_SERVER_H_

#include <OCApi.h>
#include <memory>

#include "iot/autogen/base_resource_server.h"

namespace adk {
namespace iotivity_iotsys {
static std::vector<std::string> kLicensedResources{};
static std::vector<std::string> kNonLicensedResources{kBluetoothResUri,
    kCancelBtDiscoveryModeResUri,
    kClearBtPairedListResUri,
    kConnectBtDeviceResUri,
    kDeleteCredentialResUri,
    kDisconnectBtDeviceResUri,
    kEnableBtAdapterResUri,
    kEnableVoiceUIClientResUri,
    kEnableZbAdapterResUri,
    kEnterBtDiscoveryModeResUri,
    kFactoryResetResUri,
    kFormZbNetworkResUri,
    kGetBtAdapterStateResUri,
    kGetBtPairedDevicesResUri,
    kJoinZbNetworkResUri,
    kNetworkResUri,
    kPairBtDeviceResUri,
    kRestartResUri,
    kSetZbFriendlyNameResUri,
    kStartAVSOnboardingResUri,
    kStartBtScanResUri,
    kStartZbJoiningResUri,
    kStopBtScanResUri,
    kSystemResUri,
    kUnpairBtDeviceResUri,
    kVoiceUIResUri,
    kZigbeeResUri};

struct ResourceServerCallbacks {
    struct {
        GetCb get;
        PostCb cancel_bt_discovery_mode_post;
        PostCb clear_bt_paired_list_post;
        PostCb connect_bt_device_post;
        PostCb disconnect_bt_device_post;
        PostCb enable_bt_adapter_post;
        PostCb enter_bt_discovery_mode_post;
        PostCb get_bt_adapter_state_post;
        PostCb get_bt_paired_devices_post;
        PostCb pair_bt_device_post;
        PostCb start_bt_scan_post;
        PostCb stop_bt_scan_post;
        PostCb unpair_bt_device_post;
    } bluetooth;
    struct {
        GetCb get;
    } network;
    struct {
        GetCb get;
        PostCb post;
        PostCb factory_reset_post;
        PostCb restart_post;
    } system;
    struct {
        GetCb get;
        PostCb post;
        PostCb delete_credential_post;
        PostCb enable_voice_ui_client_post;
        PostCb start_avs_onboarding_post;
    } voice_ui;
    struct {
        GetCb get;
        PostCb enable_zb_adapter_post;
        PostCb form_zb_network_post;
        PostCb join_zb_network_post;
        PostCb set_zb_friendly_name_post;
        PostCb start_zb_joining_post;
    } zigbee;
};

class ResourceServer {
 public:
    ResourceServer(const ResourceServerCallbacks& cb, bool licensed);

    void notifyBluetooth(const OC::OCRepresentation& rep);
    void notifyNetwork(const OC::OCRepresentation& rep);
    void notifySystem(const OC::OCRepresentation& rep);
    void notifyVoiceUI(const OC::OCRepresentation& rep);
    void notifyZigbee(const OC::OCRepresentation& rep);

 private:
    std::unique_ptr<BaseResourceServer> bluetooth_res_server_;
    std::unique_ptr<BaseResourceServer> cancel_bt_discovery_mode_res_server_;
    std::unique_ptr<BaseResourceServer> clear_bt_paired_list_res_server_;
    std::unique_ptr<BaseResourceServer> connect_bt_device_res_server_;
    std::unique_ptr<BaseResourceServer> disconnect_bt_device_res_server_;
    std::unique_ptr<BaseResourceServer> enable_bt_adapter_res_server_;
    std::unique_ptr<BaseResourceServer> enter_bt_discovery_mode_res_server_;
    std::unique_ptr<BaseResourceServer> get_bt_adapter_state_res_server_;
    std::unique_ptr<BaseResourceServer> get_bt_paired_devices_res_server_;
    std::unique_ptr<BaseResourceServer> pair_bt_device_res_server_;
    std::unique_ptr<BaseResourceServer> start_bt_scan_res_server_;
    std::unique_ptr<BaseResourceServer> stop_bt_scan_res_server_;
    std::unique_ptr<BaseResourceServer> unpair_bt_device_res_server_;

    std::unique_ptr<BaseResourceServer> network_res_server_;

    std::unique_ptr<BaseResourceServer> system_res_server_;
    std::unique_ptr<BaseResourceServer> factory_reset_res_server_;
    std::unique_ptr<BaseResourceServer> restart_res_server_;

    std::unique_ptr<BaseResourceServer> voice_ui_res_server_;
    std::unique_ptr<BaseResourceServer> delete_credential_res_server_;
    std::unique_ptr<BaseResourceServer> enable_voice_ui_client_res_server_;
    std::unique_ptr<BaseResourceServer> start_avs_onboarding_res_server_;

    std::unique_ptr<BaseResourceServer> zigbee_res_server_;
    std::unique_ptr<BaseResourceServer> enable_zb_adapter_res_server_;
    std::unique_ptr<BaseResourceServer> form_zb_network_res_server_;
    std::unique_ptr<BaseResourceServer> join_zb_network_res_server_;
    std::unique_ptr<BaseResourceServer> set_zb_friendly_name_res_server_;
    std::unique_ptr<BaseResourceServer> start_zb_joining_res_server_;
};
}  // namespace iotivity_iotsys
}  // namespace adk
#endif  // SRC_IOT_AUTOGEN_RESOURCE_SERVER_H_
