/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <OCApi.h>

#include "iot/autogen/resource_attr_strings.h"
#include "iot/autogen/resource_attributes_server.h"
#include "iot/autogen/resource_server.h"
#include "utils/memory.h"

namespace adk {
namespace iotivity_iotsys {

ResourceServer::ResourceServer(const ResourceServerCallbacks& cb, bool licensed) {
    bluetooth_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.get);
    bluetooth_res_server_->registerResource(
        kBluetoothResUri,
        kBluetoothResType,
        kBaselineIface,
        {kLlIface},
        OC_DISCOVERABLE | OC_OBSERVABLE);
    cancel_bt_discovery_mode_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.cancel_bt_discovery_mode_post);
    cancel_bt_discovery_mode_res_server_->registerResource(
        kCancelBtDiscoveryModeResUri,
        kCancelBtDiscoveryModeResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    clear_bt_paired_list_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.clear_bt_paired_list_post);
    clear_bt_paired_list_res_server_->registerResource(
        kClearBtPairedListResUri,
        kClearBtPairedListResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    connect_bt_device_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.connect_bt_device_post);
    connect_bt_device_res_server_->registerResource(
        kConnectBtDeviceResUri,
        kConnectBtDeviceResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    disconnect_bt_device_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.disconnect_bt_device_post);
    disconnect_bt_device_res_server_->registerResource(
        kDisconnectBtDeviceResUri,
        kDisconnectBtDeviceResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    enable_bt_adapter_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.enable_bt_adapter_post);
    enable_bt_adapter_res_server_->registerResource(
        kEnableBtAdapterResUri,
        kEnableBtAdapterResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    enter_bt_discovery_mode_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.enter_bt_discovery_mode_post);
    enter_bt_discovery_mode_res_server_->registerResource(
        kEnterBtDiscoveryModeResUri,
        kEnterBtDiscoveryModeResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    get_bt_adapter_state_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.get_bt_adapter_state_post);
    get_bt_adapter_state_res_server_->registerResource(
        kGetBtAdapterStateResUri,
        kGetBtAdapterStateResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    get_bt_paired_devices_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.get_bt_paired_devices_post);
    get_bt_paired_devices_res_server_->registerResource(
        kGetBtPairedDevicesResUri,
        kGetBtPairedDevicesResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    pair_bt_device_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.pair_bt_device_post);
    pair_bt_device_res_server_->registerResource(
        kPairBtDeviceResUri,
        kPairBtDeviceResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    start_bt_scan_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.start_bt_scan_post);
    start_bt_scan_res_server_->registerResource(
        kStartBtScanResUri,
        kStartBtScanResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    stop_bt_scan_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.stop_bt_scan_post);
    stop_bt_scan_res_server_->registerResource(
        kStopBtScanResUri,
        kStopBtScanResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    unpair_bt_device_res_server_ =
        std::make_unique<BaseResourceServer>(cb.bluetooth.unpair_bt_device_post);
    unpair_bt_device_res_server_->registerResource(
        kUnpairBtDeviceResUri,
        kUnpairBtDeviceResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);

    network_res_server_ =
        std::make_unique<BaseResourceServer>(cb.network.get);
    network_res_server_->registerResource(
        kNetworkResUri,
        kNetworkResType,
        kBaselineIface,
        {kLlIface},
        OC_DISCOVERABLE | OC_OBSERVABLE);

    system_res_server_ =
        std::make_unique<BaseResourceServer>(cb.system.get, cb.system.post);
    system_res_server_->registerResource(
        kSystemResUri,
        kSystemResType,
        kBaselineIface,
        {kLlIface},
        OC_DISCOVERABLE | OC_OBSERVABLE);
    factory_reset_res_server_ =
        std::make_unique<BaseResourceServer>(cb.system.factory_reset_post);
    factory_reset_res_server_->registerResource(
        kFactoryResetResUri,
        kFactoryResetResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    restart_res_server_ =
        std::make_unique<BaseResourceServer>(cb.system.restart_post);
    restart_res_server_->registerResource(
        kRestartResUri,
        kRestartResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);

    voice_ui_res_server_ =
        std::make_unique<BaseResourceServer>(cb.voice_ui.get, cb.voice_ui.post);
    voice_ui_res_server_->registerResource(
        kVoiceUIResUri,
        kVoiceUIResType,
        kBaselineIface,
        {kLlIface},
        OC_DISCOVERABLE | OC_OBSERVABLE);
    delete_credential_res_server_ =
        std::make_unique<BaseResourceServer>(cb.voice_ui.delete_credential_post);
    delete_credential_res_server_->registerResource(
        kDeleteCredentialResUri,
        kDeleteCredentialResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    enable_voice_ui_client_res_server_ =
        std::make_unique<BaseResourceServer>(cb.voice_ui.enable_voice_ui_client_post);
    enable_voice_ui_client_res_server_->registerResource(
        kEnableVoiceUIClientResUri,
        kEnableVoiceUIClientResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    start_avs_onboarding_res_server_ =
        std::make_unique<BaseResourceServer>(cb.voice_ui.start_avs_onboarding_post);
    start_avs_onboarding_res_server_->registerResource(
        kStartAVSOnboardingResUri,
        kStartAVSOnboardingResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);

    zigbee_res_server_ =
        std::make_unique<BaseResourceServer>(cb.zigbee.get);
    zigbee_res_server_->registerResource(
        kZigbeeResUri,
        kZigbeeResType,
        kBaselineIface,
        {kLlIface},
        OC_DISCOVERABLE | OC_OBSERVABLE);
    enable_zb_adapter_res_server_ =
        std::make_unique<BaseResourceServer>(cb.zigbee.enable_zb_adapter_post);
    enable_zb_adapter_res_server_->registerResource(
        kEnableZbAdapterResUri,
        kEnableZbAdapterResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    form_zb_network_res_server_ =
        std::make_unique<BaseResourceServer>(cb.zigbee.form_zb_network_post);
    form_zb_network_res_server_->registerResource(
        kFormZbNetworkResUri,
        kFormZbNetworkResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    join_zb_network_res_server_ =
        std::make_unique<BaseResourceServer>(cb.zigbee.join_zb_network_post);
    join_zb_network_res_server_->registerResource(
        kJoinZbNetworkResUri,
        kJoinZbNetworkResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    set_zb_friendly_name_res_server_ =
        std::make_unique<BaseResourceServer>(cb.zigbee.set_zb_friendly_name_post);
    set_zb_friendly_name_res_server_->registerResource(
        kSetZbFriendlyNameResUri,
        kSetZbFriendlyNameResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
    start_zb_joining_res_server_ =
        std::make_unique<BaseResourceServer>(cb.zigbee.start_zb_joining_post);
    start_zb_joining_res_server_->registerResource(
        kStartZbJoiningResUri,
        kStartZbJoiningResType,
        kBaselineIface,
        {},
        OC_RES_PROP_NONE);
}

void ResourceServer::notifyBluetooth(const OC::OCRepresentation& rep) {
    if (!bluetooth_res_server_) {
        return;
    }
    bluetooth_res_server_->addNotifyQueue(rep);
}
void ResourceServer::notifyNetwork(const OC::OCRepresentation& rep) {
    if (!network_res_server_) {
        return;
    }
    network_res_server_->addNotifyQueue(rep);
}
void ResourceServer::notifySystem(const OC::OCRepresentation& rep) {
    if (!system_res_server_) {
        return;
    }
    system_res_server_->addNotifyQueue(rep);
}
void ResourceServer::notifyVoiceUI(const OC::OCRepresentation& rep) {
    if (!voice_ui_res_server_) {
        return;
    }
    voice_ui_res_server_->addNotifyQueue(rep);
}
void ResourceServer::notifyZigbee(const OC::OCRepresentation& rep) {
    if (!zigbee_res_server_) {
        return;
    }
    zigbee_res_server_->addNotifyQueue(rep);
}

}  // namespace iotivity_iotsys
}  // namespace adk
