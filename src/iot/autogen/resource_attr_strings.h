/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_IOT_AUTOGEN_RESOURCE_ATTR_STRINGS_H_
#define SRC_IOT_AUTOGEN_RESOURCE_ATTR_STRINGS_H_

namespace adk {
namespace iotivity_iotsys {
// Resource Types
const char kBluetoothResType[] = "qti.iotsys.r.bluetooth";
const char kCancelBtDiscoveryModeResType[] = "qti.iotsys.r.cancelBtDiscoveryMode";
const char kClearBtPairedListResType[] = "qti.iotsys.r.clearBtPairedList";
const char kConnectBtDeviceResType[] = "qti.iotsys.r.connectBtDevice";
const char kDeleteCredentialResType[] = "qti.iotsys.r.deleteCredential";
const char kDisconnectBtDeviceResType[] = "qti.iotsys.r.disconnectBtDevice";
const char kEnableBtAdapterResType[] = "qti.iotsys.r.enableBtAdapter";
const char kEnableVoiceUIClientResType[] = "qti.iotsys.r.enableVoiceUIClient";
const char kEnableZbAdapterResType[] = "qti.iotsys.r.enableZbAdapter";
const char kEnterBtDiscoveryModeResType[] = "qti.iotsys.r.enterBtDiscoveryMode";
const char kFactoryResetResType[] = "qti.iotsys.r.factoryReset";
const char kFormZbNetworkResType[] = "qti.iotsys.r.formZbNetwork";
const char kGetBtAdapterStateResType[] = "qti.iotsys.r.getBtAdapterState";
const char kGetBtPairedDevicesResType[] = "qti.iotsys.r.getBtPairedDevices";
const char kJoinZbNetworkResType[] = "qti.iotsys.r.joinZbNetwork";
const char kNetworkResType[] = "qti.iotsys.r.network";
const char kPairBtDeviceResType[] = "qti.iotsys.r.pairBtDevice";
const char kRestartResType[] = "qti.iotsys.r.restart";
const char kSetZbFriendlyNameResType[] = "qti.iotsys.r.setZbFriendlyName";
const char kStartAVSOnboardingResType[] = "qti.iotsys.r.startAVSOnboarding";
const char kStartBtScanResType[] = "qti.iotsys.r.startBtScan";
const char kStartZbJoiningResType[] = "qti.iotsys.r.startZbJoining";
const char kStopBtScanResType[] = "qti.iotsys.r.stopBtScan";
const char kSystemResType[] = "qti.iotsys.r.system";
const char kUnpairBtDeviceResType[] = "qti.iotsys.r.unpairBtDevice";
const char kVoiceUIResType[] = "qti.iotsys.r.voiceUI";
const char kZigbeeResType[] = "qti.iotsys.r.zigbee";

// Resource Ifaces
const char kBaselineIface[] = "oic.if.baseline";
const char kLlIface[] = "oic.if.ll";

// Resource Types
const char kBluetoothResUri[] = "/qti/iotsys/bluetooth";
const char kCancelBtDiscoveryModeResUri[] = "/qti/iotsys/bluetooth/cancelBtDiscoveryMode";
const char kClearBtPairedListResUri[] = "/qti/iotsys/bluetooth/clearBtPairedList";
const char kConnectBtDeviceResUri[] = "/qti/iotsys/bluetooth/connectBtDevice";
const char kDeleteCredentialResUri[] = "/qti/iotsys/voiceUI/deleteCredential";
const char kDisconnectBtDeviceResUri[] = "/qti/iotsys/bluetooth/disconnectBtDevice";
const char kEnableBtAdapterResUri[] = "/qti/iotsys/bluetooth/enableBtAdapter";
const char kEnableVoiceUIClientResUri[] = "/qti/iotsys/voiceUI/enableVoiceUIClient";
const char kEnableZbAdapterResUri[] = "/qti/iotsys/zigbee/enableZbAdapter";
const char kEnterBtDiscoveryModeResUri[] = "/qti/iotsys/bluetooth/enterBtDiscoveryMode";
const char kFactoryResetResUri[] = "/qti/iotsys/system/factoryReset";
const char kFormZbNetworkResUri[] = "/qti/iotsys/zigbee/formZbNetwork";
const char kGetBtAdapterStateResUri[] = "/qti/iotsys/bluetooth/getBtAdapterState";
const char kGetBtPairedDevicesResUri[] = "/qti/iotsys/bluetooth/getBtPairedDevices";
const char kJoinZbNetworkResUri[] = "/qti/iotsys/zigbee/joinZbNetwork";
const char kNetworkResUri[] = "/qti/iotsys/network";
const char kPairBtDeviceResUri[] = "/qti/iotsys/bluetooth/pairBtDevice";
const char kRestartResUri[] = "/qti/iotsys/system/restart";
const char kSetZbFriendlyNameResUri[] = "/qti/iotsys/zigbee/setZbFriendlyName";
const char kStartAVSOnboardingResUri[] = "/qti/iotsys/voiceUI/startAVSOnboarding";
const char kStartBtScanResUri[] = "/qti/iotsys/bluetooth/startBtScan";
const char kStartZbJoiningResUri[] = "/qti/iotsys/zigbee/startZbJoining";
const char kStopBtScanResUri[] = "/qti/iotsys/bluetooth/stopBtScan";
const char kSystemResUri[] = "/qti/iotsys/system";
const char kUnpairBtDeviceResUri[] = "/qti/iotsys/bluetooth/unpairBtDevice";
const char kVoiceUIResUri[] = "/qti/iotsys/voiceUI";
const char kZigbeeResUri[] = "/qti/iotsys/zigbee";

// Resource Attributes keys
const char kASRProviders[] = "ASRProviders";
const char kAVSCredentialFile[] = "AVSCredentialFile";
const char kAVSLocale[] = "AVSLocale";
const char kAVSLocaleList[] = "AVSLocaleList";
const char kAVSOnboarded[] = "AVSOnboarded";
const char kAVSOnboardingError[] = "AVSOnboardingError";
const char kIPAddress[] = "IPAddress";
const char kNLUProviders[] = "NLUProviders";
const char kRSSI[] = "RSSI";
const char kSSID[] = "SSID";
const char kTTSProviders[] = "TTSProviders";
const char kAccessPoint[] = "accessPoint";
const char kAdapterState[] = "adapterState";
const char kAddress[] = "address";
const char kAuthenticateAVS[] = "authenticateAVS";
const char kBatteryPowered[] = "batteryPowered";
const char kBatteryStatus[] = "batteryStatus";
const char kBatterySupported[] = "batterySupported";
const char kChargeLevel[] = "chargeLevel";
const char kClient[] = "client";
const char kCode[] = "code";
const char kConnectedState[] = "connectedState";
const char kCoordinatorState[] = "coordinatorState";
const char kDefaultVoiceUIClient[] = "defaultVoiceUIClient";
const char kDeviceFriendlyName[] = "deviceFriendlyName";
const char kDeviceIdentifier[] = "deviceIdentifier";
const char kDeviceType[] = "deviceType";
const char kDiscoverable[] = "discoverable";
const char kEnableVoiceUI[] = "enableVoiceUI";
const char kError[] = "error";
const char kEthernetAdapter[] = "ethernetAdapter";
const char kFirmwareVersion[] = "firmwareVersion";
const char kFriendlyName[] = "friendlyName";
const char kJoinedDevices[] = "joinedDevices";
const char kJoiningState[] = "joiningState";
const char kMacAddress[] = "macAddress";
const char kManufacturer[] = "manufacturer";
const char kModel[] = "model";
const char kModularClientLanguages[] = "modularClientLanguages";
const char kName[] = "name";
const char kPairedDevices[] = "pairedDevices";
const char kPairedState[] = "pairedState";
const char kReattempt[] = "reattempt";
const char kScanResult[] = "scanResult";
const char kSelectedASRProvider[] = "selectedASRProvider";
const char kSelectedModularClientLanguage[] = "selectedModularClientLanguage";
const char kSelectedNLUProvider[] = "selectedNLUProvider";
const char kSelectedTTSProvider[] = "selectedTTSProvider";
const char kSignal[] = "signal";
const char kSignalName[] = "signalName";
const char kStatus[] = "status";
const char kTimeTillDischarge[] = "timeTillDischarge";
const char kTimeTillFullCharge[] = "timeTillFullCharge";
const char kTimeoutSecs[] = "timeoutSecs";
const char kUrl[] = "url";
const char kVersion[] = "version";
const char kVoiceUIClients[] = "voiceUIClients";
const char kWakewordStatus[] = "wakewordStatus";
const char kWifiAdapter[] = "wifiAdapter";
}  // namespace iotivity_iotsys
}  // namespace adk
#endif  // SRC_IOT_AUTOGEN_RESOURCE_ATTR_STRINGS_H_
