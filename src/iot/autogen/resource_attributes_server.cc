/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "iot/autogen/resource_attributes_server.h"

namespace adk {
namespace iotivity_iotsys {

AVSOnboardingErrorServerAttr::Error AVSOnboardingErrorServerAttr::errorFromString(const std::string& str) const {
    if (str == "timedout") {
        return Error::kTimedout;
    }
    if (str == "unknown") {
        return Error::kUnknown;
    }

    return Error::kTimedout;
}

std::string AVSOnboardingErrorServerAttr::errorToString(AVSOnboardingErrorServerAttr::Error val) const {
    switch (val) {
        case Error::kTimedout:
            return "timedout";
        case Error::kUnknown:
            return "unknown";
    }

    return "timedout";
}

OCRepresentation AVSOnboardingErrorServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kClient) != packed_attributes_.end()) {
        rep[kClient] = client_;
    }
    if (packed_attributes_.find(kError) != packed_attributes_.end()) {
        rep[kError] = errorToString(error_);
    }
    if (packed_attributes_.find(kReattempt) != packed_attributes_.end()) {
        rep[kReattempt] = reattempt_;
    }
    return rep;
}

void AVSOnboardingErrorServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kClient)) {
        unpacked_attributes_.emplace(kClient);
        client_ = rep[kClient];
    }
    if (rep.hasAttribute(kError)) {
        unpacked_attributes_.emplace(kError);
        error_ = errorFromString(rep[kError]);
    }
    if (rep.hasAttribute(kReattempt)) {
        unpacked_attributes_.emplace(kReattempt);
        reattempt_ = rep[kReattempt];
    }
}

OCRepresentation AccessPointServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kRSSI) != packed_attributes_.end()) {
        rep[kRSSI] = rssi_;
    }
    if (packed_attributes_.find(kSSID) != packed_attributes_.end()) {
        rep[kSSID] = ssid_;
    }
    return rep;
}

void AccessPointServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kRSSI)) {
        unpacked_attributes_.emplace(kRSSI);
        rssi_ = rep[kRSSI];
    }
    if (rep.hasAttribute(kSSID)) {
        unpacked_attributes_.emplace(kSSID);
        ssid_ = rep[kSSID];
    }
}

OCRepresentation AddressInServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kAddress) != packed_attributes_.end()) {
        rep[kAddress] = address_;
    }
    return rep;
}

void AddressInServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kAddress)) {
        unpacked_attributes_.emplace(kAddress);
        address_ = rep[kAddress];
    }
}

OCRepresentation AuthenticateAVSServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kCode) != packed_attributes_.end()) {
        rep[kCode] = code_;
    }
    if (packed_attributes_.find(kUrl) != packed_attributes_.end()) {
        rep[kUrl] = url_;
    }
    return rep;
}

void AuthenticateAVSServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kCode)) {
        unpacked_attributes_.emplace(kCode);
        code_ = rep[kCode];
    }
    if (rep.hasAttribute(kUrl)) {
        unpacked_attributes_.emplace(kUrl);
        url_ = rep[kUrl];
    }
}

OCRepresentation BatteryStatusServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kBatteryPowered) != packed_attributes_.end()) {
        rep[kBatteryPowered] = battery_powered_;
    }
    if (packed_attributes_.find(kChargeLevel) != packed_attributes_.end()) {
        rep[kChargeLevel] = charge_level_;
    }
    if (packed_attributes_.find(kTimeTillDischarge) != packed_attributes_.end()) {
        rep[kTimeTillDischarge] = time_till_discharge_;
    }
    if (packed_attributes_.find(kTimeTillFullCharge) != packed_attributes_.end()) {
        rep[kTimeTillFullCharge] = time_till_full_charge_;
    }
    return rep;
}

void BatteryStatusServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kBatteryPowered)) {
        unpacked_attributes_.emplace(kBatteryPowered);
        battery_powered_ = rep[kBatteryPowered];
    }
    if (rep.hasAttribute(kChargeLevel)) {
        unpacked_attributes_.emplace(kChargeLevel);
        charge_level_ = rep[kChargeLevel];
    }
    if (rep.hasAttribute(kTimeTillDischarge)) {
        unpacked_attributes_.emplace(kTimeTillDischarge);
        time_till_discharge_ = rep[kTimeTillDischarge];
    }
    if (rep.hasAttribute(kTimeTillFullCharge)) {
        unpacked_attributes_.emplace(kTimeTillFullCharge);
        time_till_full_charge_ = rep[kTimeTillFullCharge];
    }
}

OCRepresentation BtDeviceStatusServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kAddress) != packed_attributes_.end()) {
        rep[kAddress] = address_;
    }
    if (packed_attributes_.find(kName) != packed_attributes_.end()) {
        rep[kName] = name_;
    }
    if (packed_attributes_.find(kStatus) != packed_attributes_.end()) {
        rep[kStatus] = status_;
    }
    return rep;
}

void BtDeviceStatusServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kAddress)) {
        unpacked_attributes_.emplace(kAddress);
        address_ = rep[kAddress];
    }
    if (rep.hasAttribute(kName)) {
        unpacked_attributes_.emplace(kName);
        name_ = rep[kName];
    }
    if (rep.hasAttribute(kStatus)) {
        unpacked_attributes_.emplace(kStatus);
        status_ = rep[kStatus];
    }
}

OCRepresentation BtErrorServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kAddress) != packed_attributes_.end()) {
        rep[kAddress] = address_;
    }
    if (packed_attributes_.find(kStatus) != packed_attributes_.end()) {
        rep[kStatus] = status_;
    }
    return rep;
}

void BtErrorServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kAddress)) {
        unpacked_attributes_.emplace(kAddress);
        address_ = rep[kAddress];
    }
    if (rep.hasAttribute(kStatus)) {
        unpacked_attributes_.emplace(kStatus);
        status_ = rep[kStatus];
    }
}

OCRepresentation BtDeviceServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kAddress) != packed_attributes_.end()) {
        rep[kAddress] = address_;
    }
    if (packed_attributes_.find(kName) != packed_attributes_.end()) {
        rep[kName] = name_;
    }
    return rep;
}

void BtDeviceServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kAddress)) {
        unpacked_attributes_.emplace(kAddress);
        address_ = rep[kAddress];
    }
    if (rep.hasAttribute(kName)) {
        unpacked_attributes_.emplace(kName);
        name_ = rep[kName];
    }
}

BtSignalServerAttr::Name BtSignalServerAttr::nameFromString(const std::string& str) const {
    if (str == "scanStopped") {
        return Name::kScanStopped;
    }
    if (str == "pairedListCleared") {
        return Name::kPairedListCleared;
    }
    if (str == "unknown") {
        return Name::kUnknown;
    }

    return Name::kScanStopped;
}

std::string BtSignalServerAttr::nameToString(BtSignalServerAttr::Name val) const {
    switch (val) {
        case Name::kScanStopped:
            return "scanStopped";
        case Name::kPairedListCleared:
            return "pairedListCleared";
        case Name::kUnknown:
            return "unknown";
    }

    return "scanStopped";
}

OCRepresentation BtSignalServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kName) != packed_attributes_.end()) {
        rep[kName] = nameToString(name_);
    }
    if (packed_attributes_.find(kStatus) != packed_attributes_.end()) {
        rep[kStatus] = status_;
    }
    return rep;
}

void BtSignalServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kName)) {
        unpacked_attributes_.emplace(kName);
        name_ = nameFromString(rep[kName]);
    }
    if (rep.hasAttribute(kStatus)) {
        unpacked_attributes_.emplace(kStatus);
        status_ = rep[kStatus];
    }
}

BluetoothServerAttr::AdapterState BluetoothServerAttr::adapterStateFromString(const std::string& str) const {
    if (str == "enabled") {
        return AdapterState::kEnabled;
    }
    if (str == "disabled") {
        return AdapterState::kDisabled;
    }
    if (str == "unknown") {
        return AdapterState::kUnknown;
    }

    return AdapterState::kEnabled;
}

std::string BluetoothServerAttr::adapterStateToString(BluetoothServerAttr::AdapterState val) const {
    switch (val) {
        case AdapterState::kEnabled:
            return "enabled";
        case AdapterState::kDisabled:
            return "disabled";
        case AdapterState::kUnknown:
            return "unknown";
    }

    return "enabled";
}
BluetoothServerAttr::Discoverable BluetoothServerAttr::discoverableFromString(const std::string& str) const {
    if (str == "discoverable") {
        return Discoverable::kDiscoverable;
    }
    if (str == "non-discoverable") {
        return Discoverable::kNonDiscoverable;
    }
    if (str == "unknown") {
        return Discoverable::kUnknown;
    }

    return Discoverable::kDiscoverable;
}

std::string BluetoothServerAttr::discoverableToString(BluetoothServerAttr::Discoverable val) const {
    switch (val) {
        case Discoverable::kDiscoverable:
            return "discoverable";
        case Discoverable::kNonDiscoverable:
            return "non-discoverable";
        case Discoverable::kUnknown:
            return "unknown";
    }

    return "discoverable";
}

OCRepresentation BluetoothServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kAdapterState) != packed_attributes_.end()) {
        rep[kAdapterState] = adapterStateToString(adapter_state_);
    }
    if (packed_attributes_.find(kConnectedState) != packed_attributes_.end()) {
        rep[kConnectedState] = connected_state_.pack();
    }
    if (packed_attributes_.find(kDiscoverable) != packed_attributes_.end()) {
        rep[kDiscoverable] = discoverableToString(discoverable_);
    }
    if (packed_attributes_.find(kError) != packed_attributes_.end()) {
        rep[kError] = error_.pack();
    }
    if (packed_attributes_.find(kPairedDevices) != packed_attributes_.end()) {
        std::vector<OCRepresentation> reps;
        for (auto& elem : paired_devices_) {
            reps.emplace_back(elem.pack());
        }
        rep[kPairedDevices] = std::move(reps);
    }
    if (packed_attributes_.find(kPairedState) != packed_attributes_.end()) {
        rep[kPairedState] = paired_state_.pack();
    }
    if (packed_attributes_.find(kScanResult) != packed_attributes_.end()) {
        rep[kScanResult] = scan_result_.pack();
    }
    if (packed_attributes_.find(kSignal) != packed_attributes_.end()) {
        rep[kSignal] = signal_.pack();
    }
    return rep;
}

void BluetoothServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kScanResult)) {
        unpacked_attributes_.emplace(kScanResult);
        scan_result_.unpack(rep[kScanResult]);
    }
}

OCRepresentation ClientInServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kClient) != packed_attributes_.end()) {
        rep[kClient] = client_;
    }
    return rep;
}

void ClientInServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kClient)) {
        unpacked_attributes_.emplace(kClient);
        client_ = rep[kClient];
    }
}

OCRepresentation EnableBtAdapterInServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kAdapterState) != packed_attributes_.end()) {
        rep[kAdapterState] = adapter_state_;
    }
    return rep;
}

void EnableBtAdapterInServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kAdapterState)) {
        unpacked_attributes_.emplace(kAdapterState);
        adapter_state_ = rep[kAdapterState];
    }
}

OCRepresentation EnableZbAdapterInServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kAdapterState) != packed_attributes_.end()) {
        rep[kAdapterState] = adapter_state_;
    }
    return rep;
}

void EnableZbAdapterInServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kAdapterState)) {
        unpacked_attributes_.emplace(kAdapterState);
        adapter_state_ = rep[kAdapterState];
    }
}

OCRepresentation NetworkInterfaceServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kIPAddress) != packed_attributes_.end()) {
        rep[kIPAddress] = ip_address_;
    }
    if (packed_attributes_.find(kConnectedState) != packed_attributes_.end()) {
        rep[kConnectedState] = connected_state_;
    }
    if (packed_attributes_.find(kMacAddress) != packed_attributes_.end()) {
        rep[kMacAddress] = mac_address_;
    }
    return rep;
}

void NetworkInterfaceServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kIPAddress)) {
        unpacked_attributes_.emplace(kIPAddress);
        ip_address_ = rep[kIPAddress];
    }
    if (rep.hasAttribute(kConnectedState)) {
        unpacked_attributes_.emplace(kConnectedState);
        connected_state_ = rep[kConnectedState];
    }
    if (rep.hasAttribute(kMacAddress)) {
        unpacked_attributes_.emplace(kMacAddress);
        mac_address_ = rep[kMacAddress];
    }
}

OCRepresentation NetworkServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kAccessPoint) != packed_attributes_.end()) {
        rep[kAccessPoint] = access_point_.pack();
    }
    if (packed_attributes_.find(kEthernetAdapter) != packed_attributes_.end()) {
        rep[kEthernetAdapter] = ethernet_adapter_.pack();
    }
    if (packed_attributes_.find(kWifiAdapter) != packed_attributes_.end()) {
        rep[kWifiAdapter] = wifi_adapter_.pack();
    }
    return rep;
}

OCRepresentation SelectClientInServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kClient) != packed_attributes_.end()) {
        rep[kClient] = client_;
    }
    if (packed_attributes_.find(kWakewordStatus) != packed_attributes_.end()) {
        rep[kWakewordStatus] = wakeword_status_;
    }
    return rep;
}

void SelectClientInServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kClient)) {
        unpacked_attributes_.emplace(kClient);
        client_ = rep[kClient];
    }
    if (rep.hasAttribute(kWakewordStatus)) {
        unpacked_attributes_.emplace(kWakewordStatus);
        wakeword_status_ = rep[kWakewordStatus];
    }
}

OCRepresentation SetZbFriendlyNameInServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kDeviceIdentifier) != packed_attributes_.end()) {
        rep[kDeviceIdentifier] = device_identifier_;
    }
    if (packed_attributes_.find(kFriendlyName) != packed_attributes_.end()) {
        rep[kFriendlyName] = friendly_name_;
    }
    return rep;
}

void SetZbFriendlyNameInServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kDeviceIdentifier)) {
        unpacked_attributes_.emplace(kDeviceIdentifier);
        device_identifier_ = rep[kDeviceIdentifier];
    }
    if (rep.hasAttribute(kFriendlyName)) {
        unpacked_attributes_.emplace(kFriendlyName);
        friendly_name_ = rep[kFriendlyName];
    }
}

OCRepresentation SystemServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kBatteryStatus) != packed_attributes_.end()) {
        rep[kBatteryStatus] = battery_status_.pack();
    }
    if (packed_attributes_.find(kBatterySupported) != packed_attributes_.end()) {
        rep[kBatterySupported] = battery_supported_;
    }
    if (packed_attributes_.find(kDeviceFriendlyName) != packed_attributes_.end()) {
        rep[kDeviceFriendlyName] = device_friendly_name_;
    }
    if (packed_attributes_.find(kFirmwareVersion) != packed_attributes_.end()) {
        rep[kFirmwareVersion] = firmware_version_;
    }
    if (packed_attributes_.find(kManufacturer) != packed_attributes_.end()) {
        rep[kManufacturer] = manufacturer_;
    }
    if (packed_attributes_.find(kModel) != packed_attributes_.end()) {
        rep[kModel] = model_;
    }
    return rep;
}

void SystemServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kDeviceFriendlyName)) {
        unpacked_attributes_.emplace(kDeviceFriendlyName);
        device_friendly_name_ = rep[kDeviceFriendlyName];
    }
}

OCRepresentation TimeoutSecInServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kTimeoutSecs) != packed_attributes_.end()) {
        rep[kTimeoutSecs] = timeout_secs_;
    }
    return rep;
}

void TimeoutSecInServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kTimeoutSecs)) {
        unpacked_attributes_.emplace(kTimeoutSecs);
        timeout_secs_ = rep[kTimeoutSecs];
    }
}

OCRepresentation VoiceUIClientServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kName) != packed_attributes_.end()) {
        rep[kName] = name_;
    }
    if (packed_attributes_.find(kVersion) != packed_attributes_.end()) {
        rep[kVersion] = version_;
    }
    if (packed_attributes_.find(kWakewordStatus) != packed_attributes_.end()) {
        rep[kWakewordStatus] = wakeword_status_;
    }
    return rep;
}

void VoiceUIClientServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kName)) {
        unpacked_attributes_.emplace(kName);
        name_ = rep[kName];
    }
    if (rep.hasAttribute(kVersion)) {
        unpacked_attributes_.emplace(kVersion);
        version_ = rep[kVersion];
    }
    if (rep.hasAttribute(kWakewordStatus)) {
        unpacked_attributes_.emplace(kWakewordStatus);
        wakeword_status_ = rep[kWakewordStatus];
    }
}

OCRepresentation VoiceUIServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kASRProviders) != packed_attributes_.end()) {
        rep[kASRProviders] = asr_providers_;
    }
    if (packed_attributes_.find(kAVSCredentialFile) != packed_attributes_.end()) {
        rep[kAVSCredentialFile] = avs_credential_file_;
    }
    if (packed_attributes_.find(kAVSLocale) != packed_attributes_.end()) {
        rep[kAVSLocale] = avs_locale_;
    }
    if (packed_attributes_.find(kAVSLocaleList) != packed_attributes_.end()) {
        rep[kAVSLocaleList] = avs_locale_list_;
    }
    if (packed_attributes_.find(kAVSOnboarded) != packed_attributes_.end()) {
        rep[kAVSOnboarded] = avs_onboarded_;
    }
    if (packed_attributes_.find(kAVSOnboardingError) != packed_attributes_.end()) {
        rep[kAVSOnboardingError] = avs_onboarding_error_.pack();
    }
    if (packed_attributes_.find(kNLUProviders) != packed_attributes_.end()) {
        rep[kNLUProviders] = nlu_providers_;
    }
    if (packed_attributes_.find(kTTSProviders) != packed_attributes_.end()) {
        rep[kTTSProviders] = tts_providers_;
    }
    if (packed_attributes_.find(kAuthenticateAVS) != packed_attributes_.end()) {
        rep[kAuthenticateAVS] = authenticate_avs_.pack();
    }
    if (packed_attributes_.find(kDefaultVoiceUIClient) != packed_attributes_.end()) {
        rep[kDefaultVoiceUIClient] = default_voice_ui_client_;
    }
    if (packed_attributes_.find(kEnableVoiceUI) != packed_attributes_.end()) {
        rep[kEnableVoiceUI] = enable_voice_ui_;
    }
    if (packed_attributes_.find(kModularClientLanguages) != packed_attributes_.end()) {
        rep[kModularClientLanguages] = modular_client_languages_;
    }
    if (packed_attributes_.find(kSelectedASRProvider) != packed_attributes_.end()) {
        rep[kSelectedASRProvider] = selected_asr_provider_;
    }
    if (packed_attributes_.find(kSelectedModularClientLanguage) != packed_attributes_.end()) {
        rep[kSelectedModularClientLanguage] = selected_modular_client_language_;
    }
    if (packed_attributes_.find(kSelectedNLUProvider) != packed_attributes_.end()) {
        rep[kSelectedNLUProvider] = selected_nlu_provider_;
    }
    if (packed_attributes_.find(kSelectedTTSProvider) != packed_attributes_.end()) {
        rep[kSelectedTTSProvider] = selected_tts_provider_;
    }
    if (packed_attributes_.find(kVoiceUIClients) != packed_attributes_.end()) {
        std::vector<OCRepresentation> reps;
        for (auto& elem : voice_ui_clients_) {
            reps.emplace_back(elem.pack());
        }
        rep[kVoiceUIClients] = std::move(reps);
    }
    return rep;
}

void VoiceUIServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kAVSLocale)) {
        unpacked_attributes_.emplace(kAVSLocale);
        avs_locale_ = rep[kAVSLocale];
    }
    if (rep.hasAttribute(kDefaultVoiceUIClient)) {
        unpacked_attributes_.emplace(kDefaultVoiceUIClient);
        default_voice_ui_client_ = rep[kDefaultVoiceUIClient];
    }
    if (rep.hasAttribute(kEnableVoiceUI)) {
        unpacked_attributes_.emplace(kEnableVoiceUI);
        enable_voice_ui_ = rep[kEnableVoiceUI];
    }
    if (rep.hasAttribute(kSelectedASRProvider)) {
        unpacked_attributes_.emplace(kSelectedASRProvider);
        selected_asr_provider_ = rep[kSelectedASRProvider];
    }
    if (rep.hasAttribute(kSelectedModularClientLanguage)) {
        unpacked_attributes_.emplace(kSelectedModularClientLanguage);
        selected_modular_client_language_ = rep[kSelectedModularClientLanguage];
    }
    if (rep.hasAttribute(kSelectedNLUProvider)) {
        unpacked_attributes_.emplace(kSelectedNLUProvider);
        selected_nlu_provider_ = rep[kSelectedNLUProvider];
    }
    if (rep.hasAttribute(kSelectedTTSProvider)) {
        unpacked_attributes_.emplace(kSelectedTTSProvider);
        selected_tts_provider_ = rep[kSelectedTTSProvider];
    }
}

OCRepresentation ZbDeviceServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kDeviceIdentifier) != packed_attributes_.end()) {
        rep[kDeviceIdentifier] = device_identifier_;
    }
    if (packed_attributes_.find(kDeviceType) != packed_attributes_.end()) {
        rep[kDeviceType] = device_type_;
    }
    if (packed_attributes_.find(kFriendlyName) != packed_attributes_.end()) {
        rep[kFriendlyName] = friendly_name_;
    }
    return rep;
}

void ZbDeviceServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kDeviceIdentifier)) {
        unpacked_attributes_.emplace(kDeviceIdentifier);
        device_identifier_ = rep[kDeviceIdentifier];
    }
    if (rep.hasAttribute(kDeviceType)) {
        unpacked_attributes_.emplace(kDeviceType);
        device_type_ = rep[kDeviceType];
    }
    if (rep.hasAttribute(kFriendlyName)) {
        unpacked_attributes_.emplace(kFriendlyName);
        friendly_name_ = rep[kFriendlyName];
    }
}

ZbSignalServerAttr::SignalName ZbSignalServerAttr::signalNameFromString(const std::string& str) const {
    if (str == "networkFormed") {
        return SignalName::kNetworkFormed;
    }
    if (str == "networkJoined") {
        return SignalName::kNetworkJoined;
    }
    if (str == "unknown") {
        return SignalName::kUnknown;
    }

    return SignalName::kNetworkFormed;
}

std::string ZbSignalServerAttr::signalNameToString(ZbSignalServerAttr::SignalName val) const {
    switch (val) {
        case SignalName::kNetworkFormed:
            return "networkFormed";
        case SignalName::kNetworkJoined:
            return "networkJoined";
        case SignalName::kUnknown:
            return "unknown";
    }

    return "networkFormed";
}

OCRepresentation ZbSignalServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kSignalName) != packed_attributes_.end()) {
        rep[kSignalName] = signalNameToString(signal_name_);
    }
    if (packed_attributes_.find(kStatus) != packed_attributes_.end()) {
        rep[kStatus] = status_;
    }
    return rep;
}

void ZbSignalServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kSignalName)) {
        unpacked_attributes_.emplace(kSignalName);
        signal_name_ = signalNameFromString(rep[kSignalName]);
    }
    if (rep.hasAttribute(kStatus)) {
        unpacked_attributes_.emplace(kStatus);
        status_ = rep[kStatus];
    }
}

ZigbeeServerAttr::AdapterState ZigbeeServerAttr::adapterStateFromString(const std::string& str) const {
    if (str == "enabled") {
        return AdapterState::kEnabled;
    }
    if (str == "disabled") {
        return AdapterState::kDisabled;
    }
    if (str == "unknown") {
        return AdapterState::kUnknown;
    }

    return AdapterState::kEnabled;
}

std::string ZigbeeServerAttr::adapterStateToString(ZigbeeServerAttr::AdapterState val) const {
    switch (val) {
        case AdapterState::kEnabled:
            return "enabled";
        case AdapterState::kDisabled:
            return "disabled";
        case AdapterState::kUnknown:
            return "unknown";
    }

    return "enabled";
}
ZigbeeServerAttr::CoordinatorState ZigbeeServerAttr::coordinatorStateFromString(const std::string& str) const {
    if (str == "nominated") {
        return CoordinatorState::kNominated;
    }
    if (str == "notNominated") {
        return CoordinatorState::kNotNominated;
    }
    if (str == "unknown") {
        return CoordinatorState::kUnknown;
    }

    return CoordinatorState::kNominated;
}

std::string ZigbeeServerAttr::coordinatorStateToString(ZigbeeServerAttr::CoordinatorState val) const {
    switch (val) {
        case CoordinatorState::kNominated:
            return "nominated";
        case CoordinatorState::kNotNominated:
            return "notNominated";
        case CoordinatorState::kUnknown:
            return "unknown";
    }

    return "nominated";
}
ZigbeeServerAttr::JoiningState ZigbeeServerAttr::joiningStateFromString(const std::string& str) const {
    if (str == "allowed") {
        return JoiningState::kAllowed;
    }
    if (str == "disallowed") {
        return JoiningState::kDisallowed;
    }
    if (str == "unknown") {
        return JoiningState::kUnknown;
    }

    return JoiningState::kAllowed;
}

std::string ZigbeeServerAttr::joiningStateToString(ZigbeeServerAttr::JoiningState val) const {
    switch (val) {
        case JoiningState::kAllowed:
            return "allowed";
        case JoiningState::kDisallowed:
            return "disallowed";
        case JoiningState::kUnknown:
            return "unknown";
    }

    return "allowed";
}

OCRepresentation ZigbeeServerAttr::pack() const {
    OCRepresentation rep;

    if (packed_attributes_.find(kAdapterState) != packed_attributes_.end()) {
        rep[kAdapterState] = adapterStateToString(adapter_state_);
    }
    if (packed_attributes_.find(kCoordinatorState) != packed_attributes_.end()) {
        rep[kCoordinatorState] = coordinatorStateToString(coordinator_state_);
    }
    if (packed_attributes_.find(kError) != packed_attributes_.end()) {
        rep[kError] = error_;
    }
    if (packed_attributes_.find(kJoinedDevices) != packed_attributes_.end()) {
        std::vector<OCRepresentation> reps;
        for (auto& elem : joined_devices_) {
            reps.emplace_back(elem.pack());
        }
        rep[kJoinedDevices] = std::move(reps);
    }
    if (packed_attributes_.find(kJoiningState) != packed_attributes_.end()) {
        rep[kJoiningState] = joiningStateToString(joining_state_);
    }
    if (packed_attributes_.find(kSignal) != packed_attributes_.end()) {
        rep[kSignal] = signal_.pack();
    }
    return rep;
}

void ZigbeeServerAttr::unpack(const OCRepresentation& rep) {
    unpacked_attributes_.clear();
    if (rep.hasAttribute(kError)) {
        unpacked_attributes_.emplace(kError);
        error_ = rep[kError];
    }
}

}  // namespace iotivity_iotsys
}  // namespace adk
