/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "iot/autogen/base_resource_server.h"

#include <OCApi.h>
#include <OCPlatform.h>

#include "adk/log.h"
#include "adk/wakelock.h"

static const char kIotsysServerWakelock[] = "/iotsys/server/wakelock";

namespace adk {
namespace iotivity_iotsys {
using OC::EntityHandler;
using OC::ObservationInfo;
using OC::ObserveAction;
using OC::OCResourceResponse;
using OC::RequestHandlerFlag;

BaseResourceServer::~BaseResourceServer() {
    get_cb_ = nullptr;
    post_cb_ = nullptr;
    OCStackResult result = OC::OCPlatform::unregisterResource(resource_handle_);
    if (OC_STACK_OK != result) {
        ADK_LOG_ERROR("~BaseResourceServer: Unregister failed %d", result);
    }
}

bool BaseResourceServer::registerResource(std::string res_uri,
    const std::string& res_type, const std::string& res_iface,
    const std::vector<std::string> extra_ifaces,
    const uint8_t res_prop) {
    OCStackResult result = OC::OCPlatform::registerResource(resource_handle_,
        res_uri, res_type, res_iface,
        [this](std::shared_ptr<OCResourceRequest> request) -> OCEntityHandlerResult {
            if (!request) {
                return OC_EH_ERROR;
            }

            auto flag = request->getRequestHandlerFlag();
            // Register the Observation request and handle the GET request normally
            // Observation request is simple GET request with observe flag set in HeaderOptions
            if ((flag & RequestHandlerFlag::ObserverFlag) == RequestHandlerFlag::ObserverFlag) {
                observeHandler(request);
            }
            if ((flag & RequestHandlerFlag::RequestFlag) == RequestHandlerFlag::RequestFlag) {
                OCEntityHandlerResult result = OC_EH_ERROR;
                try {
                    // TODO(Ravee) - Not sure why the request handler generate
                    // an exception. This is tracked as a CR 2402887
                    result = requestHandler(request);
                } catch (OC::OCException& e) {
                    // do nothing
                }
                {
                    std::lock_guard<std::mutex> lock(notif_mutex_);
                    request_running_ = false;
                    if (!notif_rep_queue_.empty()) {
                        notify();
                    }
                }
                return result;
            }
            ADK_LOG_ERROR("entityHandler: Unsupported request flag %d", flag);
            return OC_EH_ERROR;
        },
        res_prop);

    if (OC_STACK_OK != result) {
        ADK_LOG_ERROR("registerResource: Failed to register resource %s: %d",
            res_uri.c_str(), result);
        return false;
    }

    /*Bind resource ifaces to Resource*/
    for (const auto& extra_iface : extra_ifaces) {
        (void)OC::OCPlatform::bindInterfaceToResource(resource_handle_, extra_iface);
    }
    return true;
}

void BaseResourceServer::addNotifyQueue(OCRepresentation rep) {
    ADK_LOG_INFO("BaseResourceServer::addNotifyQueue");
    std::lock_guard<std::mutex> lock(notif_mutex_);
    notif_rep_queue_.emplace(rep);
    if (!request_running_) {
        notify();
    }
}

// This function caller must be holding notif_mutex_
void BaseResourceServer::notify() {
    ADK_LOG_INFO("BaseResourceServer::notify");
    system_wake_lock_toggle(true, kIotsysServerWakelock, 0);
    if (interested_observers_.empty()) {
        std::queue<OCRepresentation>().swap(notif_rep_queue_);
        system_wake_lock_toggle(false, kIotsysServerWakelock, 0);
        return;
    }

    std::lock_guard<std::mutex> lock(base_resource_server_mutex_);
    while (!notif_rep_queue_.empty()) {
        OCRepresentation rep = notif_rep_queue_.front();
        notif_rep_queue_.pop();
        std::shared_ptr<OCResourceResponse> notifResponse =
            std::make_shared<OCResourceResponse>();
        notifResponse->setResourceRepresentation(rep);

        // It's mandatory to catch the exception here for the following reason
        // When application asks to notify the list of observers, stack internally
        // checks if the numbers of notifications sent by it matches the number of
        // observers provided the application. If there is a mismatch it raises
        // an exception, which causes abort if not caught.
        // When the clients go off the network, without informing the server, there
        // is always a mismatch.
        try {
            OC::OCPlatform::notifyListOfObservers(resource_handle_,
                interested_observers_, notifResponse);
        } catch (OC::OCException& e) {
            // Nothing to be done
        }
    }
    system_wake_lock_toggle(false, kIotsysServerWakelock, 0);
}

/*Pass on the requests to the application using the Handlers*/
OCEntityHandlerResult BaseResourceServer::requestHandler(
    const std::shared_ptr<OCResourceRequest>& request) {
    system_wake_lock_toggle(true, kIotsysServerWakelock, 0);
    {
        std::lock_guard<std::mutex> notif_lock(notif_mutex_);
        request_running_ = true;
    }
    OCEntityHandlerResult result = OC_EH_ERROR;
    auto request_type = request->getRequestType();
    auto response = std::make_shared<OC::OCResourceResponse>();
    response->setRequestHandle(request->getRequestHandle());
    response->setResourceHandle(request->getResourceHandle());

    std::lock_guard<std::mutex> lock(base_resource_server_mutex_);
    if (request_type == "GET") {
        ADK_LOG_DEBUG("BaseResourceServer::requestHandle GET %s",
            request->getResourceUri().c_str());
        OCRepresentation rep;
        result = OC_EH_METHOD_NOT_ALLOWED;

        if (get_cb_) {
            std::tie(result, rep) = get_cb_(request->getQueryParameters());
        }

        response->setResponseResult(result);
        if (result == OC_EH_OK) {
            response->setResourceRepresentation(rep);
        }
        if (OC_STACK_OK == OC::OCPlatform::sendResponse(response)) {
            ADK_LOG_DEBUG("requestHandle: Sent GET request response");
        }
    } else if (request_type == "POST") {
        /*POST can create new resource or just act like PUT*/
        ADK_LOG_DEBUG("BaseResourceServer::requestHandle POST %s",
            request->getResourceUri().c_str());
        OCRepresentation req_rep = request->getResourceRepresentation();
        OCRepresentation rep;
        result = OC_EH_METHOD_NOT_ALLOWED;

        if (post_cb_) {
            std::tie(result, rep) = post_cb_(request->getQueryParameters(), req_rep);
        }

        response->setResponseResult(result);

        if (result == OC_EH_OK) {
            response->setResourceRepresentation(rep);
        }
        if (OC_STACK_OK == OC::OCPlatform::sendResponse(response)) {
            ADK_LOG_DEBUG("requestHandle: Sent POST request response");
        }
    }
    {
        std::lock_guard<std::mutex> notif_lock(notif_mutex_);
        request_running_ = false;
    }
    // When stack clears unreachable observers, it internally create a request
    // to deregister the observer, during which the REST Method is NO_METHOD(empty)
    // so else case with log print is misleading, as if we are unable to
    // handle a request
    system_wake_lock_toggle(false, kIotsysServerWakelock, 0);
    return result;
}

OCEntityHandlerResult BaseResourceServer::observeHandler(
    const std::shared_ptr<OCResourceRequest>& request) {
    ObservationInfo observation_info = request->getObservationInfo();
    system_wake_lock_toggle(true, kIotsysServerWakelock, 0);
    if (ObserveAction::ObserveRegister == observation_info.action) {
        interested_observers_.push_back(observation_info.obsId);
    } else if (ObserveAction::ObserveUnregister == observation_info.action) {
        interested_observers_.erase(std::remove(interested_observers_.begin(),
                                        interested_observers_.end(),
                                        observation_info.obsId),
            interested_observers_.end());
    }
    system_wake_lock_toggle(false, kIotsysServerWakelock, 0);
    return OC_EH_OK;
}

}  // namespace iotivity_iotsys
}  // namespace adk
