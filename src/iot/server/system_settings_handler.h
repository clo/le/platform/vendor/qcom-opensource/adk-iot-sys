/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_IOT_SERVER_SYSTEM_SETTINGS_HANDLER_H_
#define SRC_IOT_SERVER_SYSTEM_SETTINGS_HANDLER_H_

#include "iot/server/system_settings_listener.h"

#include "iot/autogen/resource_attributes_server.h"

namespace adk {
namespace iotivity_iotsys {

class AdkIpc;
class Controller;

class SystemSettingsHandler : public SystemSettingsListener {
 public:
    explicit SystemSettingsHandler(Controller* controller);
    ~SystemSettingsHandler() override = default;

    std::tuple<OCEntityHandlerResult, OCRepresentation> getSystemSettings();
    OCEntityHandlerResult postSystemSettings(SystemServerAttr attr);
    OCEntityHandlerResult factoryReset();
    OCEntityHandlerResult restart();

 private:
    AdkIpc* adk_ipc_;
    Controller* controller_;
};

}  // namespace iotivity_iotsys
}  // namespace adk
#endif  // SRC_IOT_SERVER_SYSTEM_SETTINGS_HANDLER_H_
