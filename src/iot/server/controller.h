/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_IOT_SERVER_CONTROLLER_H_
#define SRC_IOT_SERVER_CONTROLLER_H_

#include <memory>

#include <OCApi.h>

#include "iot/autogen/resource_attributes_server.h"
#include "iot/autogen/resource_server.h"
#include "iot/server/bluetooth_settings_handler.h"
#include "iot/server/network_settings_handler.h"
#include "iot/server/system_settings_handler.h"
#include "iot/server/voiceui_settings_handler.h"
#include "iot/server/zigbee_settings_handler.h"

namespace adk {
namespace iotivity_iotsys {

using CrudReturnType = std::tuple<OCEntityHandlerResult, OCRepresentation>;

class Controller {
 public:
    explicit Controller(std::string dev_type)
        : device_type_{std::move(dev_type)} {}

    ~Controller();

    // Set up all the Resource Servers
    OCStackResult init();

    void notifyBluetoothSettings(const OC::OCRepresentation& rep);
    void notifyNetworkSettings(const OC::OCRepresentation& rep);
    void notifySystemSettings(const OC::OCRepresentation& rep);
    void notifyVoiceUISettings(const OC::OCRepresentation& rep);
    void notifyZigbeeSettings(const OC::OCRepresentation& rep);

    void setDeviceName(std::string device_name) { device_name_ = device_name; }

 private:
    OCStackResult setDeviceInfo();

    // BluetoothSettings
    CrudReturnType bluetoothGet(
        const QueryParamsMap& qp);
    CrudReturnType cancelBtDiscoveryModePost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType clearBtPairedListPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType connectBtDevicePost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType disconnectBtDevicePost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType enableBtAdapterPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType enterBtDiscoveryModePost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType getBtAdapterStatePost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType getBtPairedDevicesPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType pairBtDevicePost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType startBtScanPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType stopBtScanPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType unpairBtDevicePost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);

    // Network Settings
    CrudReturnType networkGet(
        const QueryParamsMap& qp);

    // System Settings
    CrudReturnType systemGet(
        const QueryParamsMap& qp);
    CrudReturnType systemPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType factoryResetPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType restartPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);

    // VoiceUI Settings
    CrudReturnType voiceUIGet(
        const QueryParamsMap& qp);
    CrudReturnType voiceUIPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType enableVoiceUIClientPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType startAVSOnboardingPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType deleteCredentialPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);

    // Zigbee Settings
    CrudReturnType zigbeeGet(
        const QueryParamsMap& qp);
    CrudReturnType enableZbAdapterPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType formZbNetworkPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType joinZbNetworkPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType setZbFriendlyNamePost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);
    CrudReturnType startZbJoiningPost(
        const QueryParamsMap& qp, const OCRepresentation& req_rep);

 private:
    std::unique_ptr<BluetoothSettingsHandler> bluetooth_settings_handler_;
    std::unique_ptr<NetworkSettingsHandler> network_settings_handler_;
    std::unique_ptr<SystemSettingsHandler> system_settings_handler_;
    std::unique_ptr<VoiceUISettingsHandler> voiceui_settings_handler_;
    std::unique_ptr<ZigbeeSettingsHandler> zigbee_settings_handler_;

    std::unique_ptr<ResourceServer> resource_server_;
    const std::string device_type_;
    std::string device_name_;
};
}  // namespace iotivity_iotsys
}  // namespace adk
#endif  // SRC_IOT_SERVER_CONTROLLER_H_
