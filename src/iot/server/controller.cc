/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "iot/server/controller.h"

#include <OCPlatform.h>
#include <adk/config/config.h>

#include "adk/log.h"
#include "iot/utils/stack.h"
#include "utils/memory.h"

static constexpr const char kDefaultMemberName[] = "QCS405";
static constexpr const char kConfigDeviceSettingsDbFilePath[] = "/data/adk.device.db";
namespace adk {
namespace iotivity_iotsys {

static CrudReturnType makeCrudReturn(OCEntityHandlerResult result, const OCRepresentation& rep = {}) {
    return std::make_tuple(result, rep);
}

Controller::~Controller() {
    OC::OCPlatform::stopPresence();
}

OCStackResult Controller::init() {
    std::unique_ptr<adk::config::Config> config =
        adk::config::Config::Create(kConfigDeviceSettingsDbFilePath);

    device_name_ = kDefaultMemberName;
    std::string manufacturer_name = "QTI";
    std::string firmware_version = "v1.0";
    if (config) {
        config->Read(&device_name_, "device.friendly_name");
        config->Read(&manufacturer_name, "device.manufacturer_name");
        config->Read(&firmware_version, "device.firmware_version");
    }

    // TODO(ravee) - Remove the hard-coding
    std::string platform_id = "00000000-0000-0000-0000-000000000000";

    OCPlatformInfo platform_info{const_cast<char*>(platform_id.c_str()),
        const_cast<char*>(manufacturer_name.c_str()),
        const_cast<char*>(""),
        const_cast<char*>(""),
        const_cast<char*>(""),
        const_cast<char*>(""),
        const_cast<char*>(""),
        const_cast<char*>(""),
        const_cast<char*>(firmware_version.c_str()),
        const_cast<char*>(""),
        const_cast<char*>("")};
    OCStackResult result = OC::OCPlatform::registerPlatformInfo(platform_info);
    if (result != OC_STACK_OK) {
        ADK_LOG_ERROR("Controller: Platform Registration Failed");
        return result;
    }

    result = setDeviceInfo();
    if (result != OC_STACK_OK) {
        ADK_LOG_ERROR("Controller: Device Registration Failed");
        return result;
    }

    using std::placeholders::_1;
    using std::placeholders::_2;
    ResourceServerCallbacks cb = {
        {
            std::bind(&Controller::bluetoothGet, this, _1),
            std::bind(&Controller::cancelBtDiscoveryModePost, this, _1, _2),
            std::bind(&Controller::clearBtPairedListPost, this, _1, _2),
            std::bind(&Controller::connectBtDevicePost, this, _1, _2),
            std::bind(&Controller::disconnectBtDevicePost, this, _1, _2),
            std::bind(&Controller::enableBtAdapterPost, this, _1, _2),
            std::bind(&Controller::enterBtDiscoveryModePost, this, _1, _2),
            std::bind(&Controller::getBtAdapterStatePost, this, _1, _2),
            std::bind(&Controller::getBtPairedDevicesPost, this, _1, _2),
            std::bind(&Controller::pairBtDevicePost, this, _1, _2),
            std::bind(&Controller::startBtScanPost, this, _1, _2),
            std::bind(&Controller::stopBtScanPost, this, _1, _2),
            std::bind(&Controller::unpairBtDevicePost, this, _1, _2),
        },
        {
            std::bind(&Controller::networkGet, this, _1),
        },
        {
            std::bind(&Controller::systemGet, this, _1),
            std::bind(&Controller::systemPost, this, _1, _2),
            std::bind(&Controller::factoryResetPost, this, _1, _2),
            std::bind(&Controller::restartPost, this, _1, _2),
        },
        {
            std::bind(&Controller::voiceUIGet, this, _1),
            std::bind(&Controller::voiceUIPost, this, _1, _2),
            std::bind(&Controller::deleteCredentialPost, this, _1, _2),
            std::bind(&Controller::enableVoiceUIClientPost, this, _1, _2),
            std::bind(&Controller::startAVSOnboardingPost, this, _1, _2),
        },
        {
            std::bind(&Controller::zigbeeGet, this, _1),
            std::bind(&Controller::enableZbAdapterPost, this, _1, _2),
            std::bind(&Controller::formZbNetworkPost, this, _1, _2),
            std::bind(&Controller::joinZbNetworkPost, this, _1, _2),
            std::bind(&Controller::setZbFriendlyNamePost, this, _1, _2),
            std::bind(&Controller::startZbJoiningPost, this, _1, _2),
        },
    };
    resource_server_ = std::make_unique<ResourceServer>(cb, false);
    system_settings_handler_ = std::make_unique<SystemSettingsHandler>(this);
    network_settings_handler_ = std::make_unique<NetworkSettingsHandler>(this);
    bluetooth_settings_handler_ = std::make_unique<BluetoothSettingsHandler>(this);
    voiceui_settings_handler_ = std::make_unique<VoiceUISettingsHandler>(this);
    zigbee_settings_handler_ = std::make_unique<ZigbeeSettingsHandler>(this);

    OC::OCPlatform::startPresence(0);
    return OC_STACK_OK;
}

void Controller::notifyBluetoothSettings(const OC::OCRepresentation& rep) {
    ADK_LOG_INFO("Controller::notifyBluetoothSettings");
    resource_server_->notifyBluetooth(rep);
}

void Controller::notifyNetworkSettings(const OC::OCRepresentation& rep) {
    ADK_LOG_INFO("Controller::notifyNetworkSettings");
    resource_server_->notifyNetwork(rep);
}

void Controller::notifySystemSettings(const OC::OCRepresentation& rep) {
    ADK_LOG_INFO("Controller::notifySystemSettings");
    resource_server_->notifySystem(rep);
}

void Controller::notifyVoiceUISettings(const OC::OCRepresentation& rep) {
    ADK_LOG_INFO("Controller::notifyVoiceUISettings");
    resource_server_->notifyVoiceUI(rep);
}

void Controller::notifyZigbeeSettings(const OC::OCRepresentation& rep) {
    ADK_LOG_INFO("Controller::notifyZigbeeSettings");
    resource_server_->notifyZigbee(rep);
}

OCStackResult Controller::setDeviceInfo() {
    OCStackResult result = OC_STACK_ERROR;

    OCResourceHandle handle = OCGetResourceHandleAtUri(OC_RSRVD_DEVICE_URI);
    if (handle == nullptr) {
        ADK_LOG_ERROR("setDeviceInfo: Failed to find resource %s", OC_RSRVD_DEVICE_URI);
        return result;
    }

    result = OCBindResourceTypeToResource(handle, device_type_.c_str());
    if (result != OC_STACK_OK) {
        ADK_LOG_ERROR("setDeviceInfo: Failed to add device type");
        return result;
    }

    result = OC::OCPlatform::setPropertyValue(PAYLOAD_TYPE_DEVICE,
        OC_RSRVD_DEVICE_NAME, device_name_);
    if (result != OC_STACK_OK) {
        ADK_LOG_ERROR("setDeviceInfo: Failed to set device name");
        return result;
    }

    result = OC::OCPlatform::setPropertyValue(
        PAYLOAD_TYPE_DEVICE, OC_RSRVD_DATA_MODEL_VERSION, OC_DATA_MODEL_VERSION);
    if (result != OC_STACK_OK) {
        ADK_LOG_ERROR("setDeviceInfo: Failed to set data model versions");
        return result;
    }

    result = OC::OCPlatform::setPropertyValue(
        PAYLOAD_TYPE_DEVICE, OC_RSRVD_SPEC_VERSION, OC_SPEC_VERSION);
    if (result != OC_STACK_OK) {
        ADK_LOG_ERROR("setDeviceInfo: Failed to set spec version");
        return result;
    }

    result = OC::OCPlatform::setPropertyValue(
        PAYLOAD_TYPE_DEVICE, OC_RSRVD_PROTOCOL_INDEPENDENT_ID,
        "00000000-0000-0000-0000-000000000000");
    if (result != OC_STACK_OK) {
        ADK_LOG_ERROR("setDeviceInfo: Failed to set piid");
        return result;
    }

    return OC_STACK_OK;
}

CrudReturnType Controller::bluetoothGet(const QueryParamsMap& /*qp*/) {
    ADK_LOG_INFO("Controller::bluetoothGet");
    if (!bluetooth_settings_handler_->getBTSettings()) {
        return makeCrudReturn(OC_EH_ERROR);
    }
    return makeCrudReturn(OC_EH_OK);
}

CrudReturnType Controller::cancelBtDiscoveryModePost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& /*req_rep*/) {
    ADK_LOG_INFO("Controller::cancelDiscoveryModePost");
    return makeCrudReturn(bluetooth_settings_handler_->cancelDiscoveryMode());
}

CrudReturnType Controller::clearBtPairedListPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& /*req_rep*/) {
    ADK_LOG_INFO("Controller::clearPairListPost");
    return makeCrudReturn(bluetooth_settings_handler_->clearPairedList());
}

CrudReturnType Controller::connectBtDevicePost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::connectPost");
    AddressInServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(bluetooth_settings_handler_->connect(attr));
}

CrudReturnType Controller::disconnectBtDevicePost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::disconnectPost");
    AddressInServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(bluetooth_settings_handler_->disconnect(attr));
}

CrudReturnType Controller::enableBtAdapterPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::enableBluetoothPost");
    EnableBtAdapterInServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(bluetooth_settings_handler_->enable(attr));
}

CrudReturnType Controller::enterBtDiscoveryModePost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::enterDiscoveryModePost");
    TimeoutSecInServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(bluetooth_settings_handler_->enterDiscoveryMode(attr));
}

CrudReturnType Controller::getBtAdapterStatePost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& /*req_rep*/) {
    ADK_LOG_INFO("Controller::getBtAdapterStatePost");
    return makeCrudReturn(bluetooth_settings_handler_->getBtAdapterState());
}

CrudReturnType Controller::getBtPairedDevicesPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& /*req_rep*/) {
    ADK_LOG_INFO("Controller::getPairedDevicesPost");
    return makeCrudReturn(bluetooth_settings_handler_->getPairedDevices());
}

CrudReturnType Controller::pairBtDevicePost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::pairPost");
    AddressInServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(bluetooth_settings_handler_->pair(attr));
}

CrudReturnType Controller::startBtScanPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& /*req_rep*/) {
    ADK_LOG_INFO("Controller::startScanPost");
    return makeCrudReturn(bluetooth_settings_handler_->startScan());
}

CrudReturnType Controller::stopBtScanPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& /*req_rep*/) {
    ADK_LOG_INFO("Controller::stopScanPost");
    return makeCrudReturn(bluetooth_settings_handler_->stopScan());
}

CrudReturnType Controller::unpairBtDevicePost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::unpairPost");
    AddressInServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(bluetooth_settings_handler_->unpair(attr));
}

CrudReturnType Controller::networkGet(const QueryParamsMap& /*qp*/) {
    ADK_LOG_INFO("Controller::networkGet");
    if (!network_settings_handler_->getNetworkStateInfo()) {
        return makeCrudReturn(OC_EH_ERROR);
    }
    return makeCrudReturn(OC_EH_OK);
}

CrudReturnType Controller::systemGet(const QueryParamsMap& /*qp*/) {
    ADK_LOG_INFO("Controller::systemGet");
    return system_settings_handler_->getSystemSettings();
}

CrudReturnType Controller::systemPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::systemPost");
    SystemServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(system_settings_handler_->postSystemSettings(attr));
}

CrudReturnType Controller::factoryResetPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& /*req_rep*/) {
    ADK_LOG_INFO("Controller::factoryResetPost");
    return makeCrudReturn(system_settings_handler_->factoryReset());
}

CrudReturnType Controller::restartPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& /*req_rep*/) {
    ADK_LOG_INFO("Controller::restartPost");
    return makeCrudReturn(system_settings_handler_->restart());
}

CrudReturnType Controller::voiceUIGet(
    const QueryParamsMap& /*qp*/) {
    ADK_LOG_INFO("Controller::voiceUIGet");
    return voiceui_settings_handler_->getVoiceUISettings();
}

CrudReturnType Controller::voiceUIPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::voiceUIPost");
    VoiceUIServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(voiceui_settings_handler_->postVoiceUISettings(attr));
}

CrudReturnType Controller::enableVoiceUIClientPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::enableVoiceUIClientPost");
    SelectClientInServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(voiceui_settings_handler_->enableVoiceUIClient(attr));
}

CrudReturnType Controller::startAVSOnboardingPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& /*req_rep*/) {
    ADK_LOG_INFO("Controller::startAVSOnboardingPost");
    return makeCrudReturn(voiceui_settings_handler_->startAVSOnboarding());
}

CrudReturnType Controller::deleteCredentialPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::deleteCredentialPost");
    ClientInServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(voiceui_settings_handler_->deleteCredential(attr));
}

CrudReturnType Controller::zigbeeGet(
    const QueryParamsMap& /*qp*/) {
    ADK_LOG_INFO("Controller::zigbeeGet");
    if (!zigbee_settings_handler_->getZigbeeSettings()) {
        return makeCrudReturn(OC_EH_ERROR);
    }
    return makeCrudReturn(OC_EH_OK);
}

CrudReturnType Controller::enableZbAdapterPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::enableZbAdapterPost");
    EnableZbAdapterInServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(zigbee_settings_handler_->enable(attr));
}

CrudReturnType Controller::formZbNetworkPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& /*req_rep*/) {
    ADK_LOG_INFO("Controller::formZbNetworkPost");
    return makeCrudReturn(zigbee_settings_handler_->formNetwork());
}

CrudReturnType Controller::joinZbNetworkPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& /*req_rep*/) {
    ADK_LOG_INFO("Controller::joinZbNetworkPost");
    return makeCrudReturn(zigbee_settings_handler_->joinNetwork());
}

CrudReturnType Controller::setZbFriendlyNamePost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::setZbFriendlyNamePost");
    SetZbFriendlyNameInServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(zigbee_settings_handler_->setFriendlyName(attr));
}

CrudReturnType Controller::startZbJoiningPost(
    const QueryParamsMap& /*qp*/, const OCRepresentation& req_rep) {
    ADK_LOG_INFO("Controller::startZbJoiningPost");
    TimeoutSecInServerAttr attr;
    attr.unpack(req_rep);
    return makeCrudReturn(zigbee_settings_handler_->startJoining(attr));
}
}  // namespace iotivity_iotsys
}  // namespace adk
