/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "iot/server/system_settings_handler.h"

#include <adk/config/config.h>

#include "adk/log.h"
#include "adk_ipc/adk_ipc.h"
#include "iot/server/controller.h"
#include "iot/utils/config-keys.h"

namespace adk {
namespace iotivity_iotsys {

const int kSystemSettingsDummyNumber = 100;

SystemSettingsHandler::SystemSettingsHandler(Controller* controller)
    : adk_ipc_(AdkIpc::getInstance()), controller_(controller) {
    ADK_LOG_INFO("SystemSettingsHandler::SystemSettingsHandler");
}

std::tuple<OCEntityHandlerResult, OCRepresentation>
SystemSettingsHandler::getSystemSettings() {
    ADK_LOG_INFO("SystemSettingsHandler::getSystemSettings");

    OCRepresentation rep = {};
    std::unique_ptr<adk::config::Config> config =
        adk::config::Config::Create(DEVICE_CONFIG_FILENAME);
    if (!config) {
        ADK_LOG_ERROR("SystemSettingsHandler:getSystemSettings"
            "Failed to initialize the SystemSettings config database: %s",
            DEVICE_CONFIG_FILENAME);
        return std::make_tuple(OC_EH_ERROR, rep);
    }

    SystemServerAttr attr;
    std::string device_friendly_name;
    if (config->Read(&device_friendly_name, adk::device::keys::kDeviceFriendlyName)) {
        attr.setDeviceFriendlyName(device_friendly_name);
    }

    std::string firmware_version;
    if (config->Read(&firmware_version, adk::device::keys::kFirmwareVersion)) {
        attr.setFirmwareVersion(firmware_version);
    }

    std::string manufacturer;
    if (config->Read(&manufacturer, adk::device::keys::kManufacturerName)) {
        attr.setManufacturer(manufacturer);
    }

    std::string model_name;
    if (config->Read(&model_name, adk::device::keys::kModel)) {
        attr.setModel(model_name);
    }

    BatteryStatusServerAttr bat_attr;
    bat_attr.setBatteryPowered(true);
    bat_attr.setChargeLevel(kSystemSettingsDummyNumber);        // percentage
    bat_attr.setTimeTillDischarge(kSystemSettingsDummyNumber);  // seconds
    attr.setBatteryStatus(bat_attr);

    return std::make_tuple(OC_EH_OK, attr.pack());
}

OCEntityHandlerResult SystemSettingsHandler::postSystemSettings(SystemServerAttr attr) {
    ADK_LOG_INFO("SystemSettingsHandler::postSystemSettings");

    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();
    if (unpacked.find(std::string(kDeviceFriendlyName)) == unpacked.end()) {
        ADK_LOG_ERROR("SystemSettingsHandler::postSystemSettings DeviceFriendlyName attribute not found");
        return OC_EH_BAD_REQ;
    }
    std::string device_friendly_name = attr.getDeviceFriendlyName();
    bool enable_bt_friendly_name;
    if (device_friendly_name.empty()) {
        ADK_LOG_ERROR("INVALID REQUEST");
        return OC_EH_BAD_REQ;
    }

    std::unique_ptr<adk::config::Config> config =
        adk::config::Config::Create(DEVICE_CONFIG_FILENAME);
    if (!config) {
        ADK_LOG_ERROR("SystemSettingsHandler::postSystemSettings"
            "Failed to initialize the SystemSettings config database: %s",
            DEVICE_CONFIG_FILENAME);
        return OC_EH_ERROR;
    }

    if (!config->Write(device_friendly_name, adk::device::keys::kDeviceFriendlyName)) {
        ADK_LOG_ERROR(
            "SystemSettingsHandler::postSystemSettings device_friendly_name- "
            "Error Writing Database");
    }

    if (!adk_ipc_->deviceFriendlyNameChanged(device_friendly_name)) {
        ADK_LOG_ERROR("SystemSettingsHandler::PostSystemSettings Failed to send deviceFriendlyNameChanged signal");
        return OC_EH_ERROR;
    }

    SystemServerAttr notification_attr;
    notification_attr.setDeviceFriendlyName(device_friendly_name);
    controller_->notifySystemSettings(notification_attr.pack());
    controller_->setDeviceName(device_friendly_name);

    if (!config->Read(&enable_bt_friendly_name, adk::device::keys::kEnableBTFriendlyName)) {
        ADK_LOG_ERROR(
            "SystemSettingsHandler::postSystemSettings enable_bt_friendly_name- "
            "Error reading Database");
        enable_bt_friendly_name = false;
    }

    if (enable_bt_friendly_name) {
        if (!adk_ipc_->setBTDeviceName(device_friendly_name)) {
            ADK_LOG_ERROR(
                "SystemSettingsHandler::PostSystemSettings "
                "Failed to send btSetName signal");
        } else {
            BluetoothServerAttr btser_attr;
            controller_->notifyBluetoothSettings(btser_attr.pack());
        }
    }

    return OC_EH_OK;
}

OCEntityHandlerResult SystemSettingsHandler::factoryReset() {
    // ADK_LOG_INFO("SystemSettingsHandler::factoryReset");
    // return OC_EH_OK;

    // TO DO :: implement factory reset
    ADK_LOG_ERROR("SystemSettingsHandler::factoryReset Not Supported");
    return OC_EH_ERROR;
}

OCEntityHandlerResult SystemSettingsHandler::restart() {
    // ADK_LOG_INFO("SystemSettingsHandler::restart");
    // return OC_EH_OK;

    // TO DO :: implement restart
    ADK_LOG_ERROR("SystemSettingsHandler::restart Not Supported");
    return OC_EH_ERROR;
}

}  // namespace iotivity_iotsys
}  // namespace adk
