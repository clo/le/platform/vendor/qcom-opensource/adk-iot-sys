/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "iot/server/zigbee_settings_handler.h"

#include "adk/log.h"
#include "adk_ipc/adk_ipc.h"
#include "iot/server/controller.h"

namespace adk {
namespace iotivity_iotsys {

ZigbeeSettingsHandler::ZigbeeSettingsHandler(Controller* controller)
    : adk_ipc_(AdkIpc::getInstance()), controller_(controller) {
    ADK_LOG_INFO("ZigbeeSettingsHandler::ZigbeeSettingsHandler");
    adk_ipc_->registerZigbeeSettingsListener(this);
}

bool ZigbeeSettingsHandler::getZigbeeSettings() {
    ADK_LOG_INFO("ZigbeeSettingsHandler::getZigbeeSettings");
    if (!adk_ipc_->getZbAdapterStatus()) {
        return false;
    }

    if (!adk_ipc_->getZbCoordinatorStatus()) {
        return false;
    }

    if (!adk_ipc_->getZbDeviceList()) {
        return false;
    }

    return true;
}

OCEntityHandlerResult ZigbeeSettingsHandler::enable(EnableZbAdapterInServerAttr attr) {
    ADK_LOG_INFO("ZigbeeSettingsHandler::enable");
    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();
    if (unpacked.find(std::string(kAdapterState)) == unpacked.end()) {
        ADK_LOG_ERROR("ZigbeeSettingsHandler::enable adapter state attribute not found");
        return OC_EH_BAD_REQ;
    }
    if (!adk_ipc_->enableZb(attr.getAdapterState())) {
        return OC_EH_ERROR;
    }

    return OC_EH_OK;
}

OCEntityHandlerResult ZigbeeSettingsHandler::formNetwork() {
    ADK_LOG_INFO("ZigbeeSettingsHandler::formNetwork");
    if (!adk_ipc_->formZbNetwork()) {
        return OC_EH_ERROR;
    }

    return OC_EH_OK;
}

OCEntityHandlerResult ZigbeeSettingsHandler::joinNetwork() {
    ADK_LOG_INFO("ZigbeeSettingsHandler::joinNetwork");
    if (!adk_ipc_->joinZbNetwork()) {
        return OC_EH_ERROR;
    }
    return OC_EH_OK;
}

OCEntityHandlerResult ZigbeeSettingsHandler::setFriendlyName(SetZbFriendlyNameInServerAttr attr) {
    ADK_LOG_INFO("ZigbeeSettingsHandler::setFriendlyName");
    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();
    if (unpacked.find(std::string(kDeviceIdentifier)) == unpacked.end() || unpacked.find(std::string(kFriendlyName)) == unpacked.end()) {
        ADK_LOG_ERROR("ZigbeeSettingsHandler::setFriendlyName attributes not found");
        return OC_EH_BAD_REQ;
    }
    int64_t device_identifier = attr.getDeviceIdentifier();
    std::string friendly_name = attr.getFriendlyName();
    if (device_identifier < 0 || friendly_name.empty()) {
        ADK_LOG_ERROR("ZigbeeSettingsHandler::setFriendlyName Invalid Attributes");
        return OC_EH_BAD_REQ;
    }
    if (!adk_ipc_->setZbFriendlyName(device_identifier, friendly_name)) {
        return OC_EH_ERROR;
    }
    return OC_EH_OK;
}

OCEntityHandlerResult ZigbeeSettingsHandler::startJoining(TimeoutSecInServerAttr attr) {
    ADK_LOG_INFO("ZigbeeSettingsHandler::startJoining");
    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();
    if (unpacked.find(std::string(kTimeoutSecs)) == unpacked.end()) {
        ADK_LOG_ERROR("ZigbeeSettingsHandler::startJoining TimeOutSecs attribute not found");
        return OC_EH_BAD_REQ;
    }
    int timeout_secs = attr.getTimeoutSecs();
    if (timeout_secs <= 0) {
        ADK_LOG_ERROR("ZigbeeSettingsHandler::startJoining Invalid TimeOutSecs");
        return OC_EH_BAD_REQ;
    }
    if (!adk_ipc_->startZbJoining(timeout_secs)) {
        return OC_EH_ERROR;
    }
    return OC_EH_OK;
}

void ZigbeeSettingsHandler::zbAdapterState(bool enabled) {
    ADK_LOG_INFO("ZigbeeSettingsHandler::zbAdapterState");
    ZigbeeServerAttr attr;
    if (enabled) {
        attr.setAdapterState(ZigbeeServerAttr::AdapterState::kEnabled);
    } else {
        attr.setAdapterState(ZigbeeServerAttr::AdapterState::kDisabled);
    }
    controller_->notifyZigbeeSettings(attr.pack());
}

void ZigbeeSettingsHandler::zbCoordinatorState(bool coordinator_state) {
    ADK_LOG_INFO("ZigbeeSettingsHandler::zbCoordinatorState");
    ZigbeeServerAttr attr;
    if (coordinator_state) {
        attr.setCoordinatorState(ZigbeeServerAttr::CoordinatorState::kNominated);
    } else {
        attr.setCoordinatorState(ZigbeeServerAttr::CoordinatorState::kNotNominated);
    }
    controller_->notifyZigbeeSettings(attr.pack());
}

void ZigbeeSettingsHandler::zbJoiningState(bool joining_state) {
    ADK_LOG_INFO("ZigbeeSettingsHandler::zbJoiningState");
    ZigbeeServerAttr attr;
    if (joining_state) {
        attr.setJoiningState(ZigbeeServerAttr::JoiningState::kAllowed);
    } else {
        attr.setJoiningState(ZigbeeServerAttr::JoiningState::kDisallowed);
    }
    controller_->notifyZigbeeSettings(attr.pack());
}

void ZigbeeSettingsHandler::zbJoinedDevices(std::vector<ZigbeeDevice> joined_devices) {
    ADK_LOG_INFO("ZigbeeSettingsHandler::zbJoinedDevices");
    ZigbeeServerAttr attr;
    std::vector<ZbDeviceServerAttr> joined_zb_devices;

    for (auto& it : joined_devices) {
        ZbDeviceServerAttr attr2;
        attr2.setDeviceIdentifier(it.device_identifier);
        attr2.setFriendlyName(it.friendly_name);
        attr2.setDeviceType(it.device_type);
        joined_zb_devices.emplace_back(attr2);
    }

    attr.setJoinedDevices(joined_zb_devices);
    controller_->notifyZigbeeSettings(attr.pack());
}

void ZigbeeSettingsHandler::zbNetworkFormed(bool status) {
    ADK_LOG_INFO("ZigbeeSettingsHandler::zbJoiningStopped");
    ZigbeeServerAttr attr;
    ZbSignalServerAttr signal_attr;
    signal_attr.setSignalName(ZbSignalServerAttr::SignalName::kNetworkFormed);
    signal_attr.setStatus(status);
    attr.setSignal(signal_attr);
    controller_->notifyZigbeeSettings(attr.pack());
}

void ZigbeeSettingsHandler::zbNetworkJoined(bool status) {
    ADK_LOG_INFO("ZigbeeSettingsHandler::zbJoiningStarted");
    ZigbeeServerAttr attr;
    ZbSignalServerAttr signal_attr;
    signal_attr.setSignalName(ZbSignalServerAttr::SignalName::kNetworkJoined);
    signal_attr.setStatus(status);
    attr.setSignal(signal_attr);
    controller_->notifyZigbeeSettings(attr.pack());
}

void ZigbeeSettingsHandler::zbError(const std::string& status) {
    ADK_LOG_INFO("ZigbeeSettingsHandler::zbError");
    ZigbeeServerAttr attr;
    attr.setError(status);
    controller_->notifyZigbeeSettings(attr.pack());
}
}  // namespace iotivity_iotsys
}  // namespace adk
