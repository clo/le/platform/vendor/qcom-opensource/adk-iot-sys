/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "iot/server/network_settings_handler.h"

#include <arpa/inet.h>
#include <ifaddrs.h>
#include <linux/wireless.h>
#include <sys/ioctl.h>
#include <fstream>

#include "adk/log.h"
#include "adk_ipc/adk_ipc.h"
#include "iot/server/controller.h"

namespace adk {
namespace iotivity_iotsys {

NetworkSettingsHandler::NetworkSettingsHandler(Controller* controller)
    : adk_ipc_(AdkIpc::getInstance()), controller_(controller) {
    ADK_LOG_INFO("NetworkSettingsHandler::NetworkSettingsHandler");
    adk_ipc_->registerNetworkSettingsListener(this);
}

bool NetworkSettingsHandler::getNetworkStateInfo() {
    ADK_LOG_INFO("NetworkSettingsHandler::getNetworkStateInfo");

    if (!adk_ipc_->getNetworkStateInfo()) {
        return false;
    }

    return true;
}

void NetworkSettingsHandler::networkStateInfo(NetworkInfoReceived info) {
    ADK_LOG_INFO("NetworkSettingsHandler::networkStateInfo");

    NetworkServerAttr attr = {};

    if (info.received_connectivity) {
        if (info.connectivity == "wifi") {
            AccessPointServerAttr access_point_attr = {};
            NetworkInterfaceServerAttr wifi_adapter_attr = {};

            if (info.received_ssid)
                access_point_attr.setSSID(info.ssid);
            if (info.received_rssi)
                access_point_attr.setRSSI(std::to_string(info.rssi));
            attr.setAccessPoint(access_point_attr);

            if (info.received_mac_address)
                wifi_adapter_attr.setMacAddress(info.mac_address);
            if (info.received_ip_address) {
                wifi_adapter_attr.setIPAddress(info.ip_address);
                if (!info.ip_address.empty())
                    wifi_adapter_attr.setConnectedState(true);
                else
                    wifi_adapter_attr.setConnectedState(false);
            }
            attr.setWifiAdapter(wifi_adapter_attr);
        } else if (info.connectivity == "ethernet") {
            NetworkInterfaceServerAttr eth_adapter_attr = {};

            if (info.received_mac_address)
                eth_adapter_attr.setMacAddress(info.mac_address);
            if (info.received_ip_address) {
                eth_adapter_attr.setIPAddress(info.ip_address);
                if (!info.ip_address.empty())
                    eth_adapter_attr.setConnectedState(true);
                else
                    eth_adapter_attr.setConnectedState(false);
            }
            attr.setEthernetAdapter(eth_adapter_attr);
        }
    }

    controller_->notifyNetworkSettings(attr.pack());
}

}  // namespace iotivity_iotsys
}  // namespace adk
