/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "iot/server/voiceui_settings_handler.h"

#include <adk/config/config.h>
#include <sstream>

#include "adk/log.h"
#include "adk_ipc/adk_ipc.h"
#include "iot/server/controller.h"
#include "iot/utils/helper.h"
#include "iot/utils/config-keys.h"

namespace adk {
namespace iotivity_iotsys {

VoiceUISettingsHandler::VoiceUISettingsHandler(Controller* controller)
    : adk_ipc_(AdkIpc::getInstance()), controller_(controller) {
    ADK_LOG_INFO("VoiceUISettingsHandler::VoiceUISettingsHandler");
    adk_ipc_->registerVoiceUISettingsListener(this);
}

std::tuple<OCEntityHandlerResult, OCRepresentation>
VoiceUISettingsHandler::getVoiceUISettings() {
    ADK_LOG_INFO("VoiceUISettingsHandler::getVoiceUISettings");

    OCRepresentation rep = {};
    std::unique_ptr<adk::config::Config> config =
        adk::config::Config::Create(VOICEUI_CONFIG_FILENAME);
    if (!config) {
        ADK_LOG_ERROR("VoiceUISettingsHandler::getVoiceUISettings"
            "Failed to initialize the VoiceUI config database: %s",
            VOICEUI_CONFIG_FILENAME);
        return std::make_tuple(OC_EH_ERROR, rep);
    }

    VoiceUIServerAttr attr;

    bool enable_voiceui;
    if (config->Read(&enable_voiceui, voiceUIFramework::voiceUIUtils::Keys::k_framework_status_)) {
        attr.setEnableVoiceUI(enable_voiceui);
    }

    std::vector<VoiceUIClientServerAttr> voiceui_clients = getVoiceUIClients();
    if (!voiceui_clients.empty()) {
        attr.setVoiceUIClients(voiceui_clients);
    }

    std::string default_voice_ui_client;
    if (config->Read(&default_voice_ui_client, voiceUIFramework::voiceUIUtils::Keys::k_framework_default_client_)) {
        attr.setDefaultVoiceUIClient(default_voice_ui_client);
    }

    // TODO(Shravya): Remove this hardcoding when config read for enum is available
    std::vector<std::string> avs_locale_list{"en-US", "en-GB", "de-DE", "en-IN", "en-CA", "ja-JP", "en-AU", "fr-FR"};
    attr.setAVSLocaleList(avs_locale_list);

    std::string avs_locale;
    if (config->Read(&avs_locale, voiceUIFramework::voiceUIUtils::Keys::k_avs_locale_)) {
        attr.setAVSLocale(avs_locale);
    }

    std::string avs_credential_file;
    if (config->Read(&avs_credential_file, voiceUIFramework::voiceUIUtils::Keys::k_avs_credential_file_)) {
        attr.setAVSCredentialFile(avs_credential_file);
    }

    bool avs_onboarded;
    if (config->Read(&avs_onboarded, voiceUIFramework::voiceUIUtils::Keys::k_avs_onboarded_)) {
        attr.setAVSOnboarded(avs_onboarded);
    }

    // TODO(Shravya): Remove this hardcoding when config read for enum is available
    std::vector<std::string> asr_providers{"none", "QTI Local ASR"};
    attr.setASRProviders(asr_providers);

    std::vector<std::string> nlu_providers{"none"};
    attr.setNLUProviders(nlu_providers);

    std::vector<std::string> tts_providers{"none", "local files"};
    attr.setTTSProviders(tts_providers);

    std::string selected_asr_provider;
    if (config->Read(&selected_asr_provider, voiceUIFramework::voiceUIUtils::Keys::k_modular_asr_selected_ )) {
        attr.setSelectedASRProvider(selected_asr_provider);
    }

    std::string selected_nlu_provider;
    if (config->Read(&selected_nlu_provider, voiceUIFramework::voiceUIUtils::Keys::k_modular_nlu_selected_ )) {
        attr.setSelectedNLUProvider(selected_nlu_provider);
    }

    std::string selected_tts_provider;
    if (config->Read(&selected_tts_provider, voiceUIFramework::voiceUIUtils::Keys::k_modular_tts_selected_)) {
        attr.setSelectedTTSProvider(selected_tts_provider);
    }
    return std::make_tuple(OC_EH_OK, attr.pack());
}

std::vector<VoiceUIClientServerAttr> VoiceUISettingsHandler::getVoiceUIClients() {
    ADK_LOG_INFO("VoiceUISettingsHandler::getVoiceUIClients");

    std::unique_ptr<adk::config::Config> config =
        adk::config::Config::Create(VOICEUI_CONFIG_FILENAME);
    if (!config) {
        ADK_LOG_ERROR(
            "VoiceUISettingsHandler::getVoiceUIClients"
            "Failed to initialize the VoiceUI config database: %s",
            VOICEUI_CONFIG_FILENAME);
        return {};
    }

    std::vector<VoiceUIClientServerAttr> voiceui_clients;
    VoiceUIClientServerAttr client_attr;

    // TODO(Shravya): Remove the hardcoding when config read for enum is available
    bool enable_client = false;
    // check AVS support
    if (config->Read(&enable_client, voiceUIFramework::voiceUIUtils::Keys::k_avs_status_)) {
        if (enable_client) {
            client_attr.setName("AVS");
            client_attr.setVersion("1.10");
            bool wakeword_status;
            if (config->Read(&wakeword_status, voiceUIFramework::voiceUIUtils::Keys::k_avs_wakeword_status_)) {
                client_attr.setWakewordStatus(wakeword_status);
            }
            voiceui_clients.emplace_back(client_attr);
        }
    }

    // TODO(Shravya): Enable this when the keys are available for these clients
    /*
    // check GVA support
    if (config->Read(&enable_client, voiceUIFramework::voiceUIUtils::Keys::k_gva_status_)) {
        if (enable_client) {
            client_attr.setName("GVA");
            client_attr.setVersion("0.1");
            bool wakeword_status;
            if (config->Read(&wakeword_status, voiceUIFramework::voiceUIUtils::Keys::k_gva_wakeword_status_)) {
                client_attr.setWakewordStatus(wakeword_status);
            }
            voiceui_clients.emplace_back(client_attr);
            enable_client = false;
        }
    }
    // check cortana support
    if (config->Read(&enable_client, voiceUIFramework::voiceUIUtils::Keys::k_cortana_status_)) {
        if (enable_client) {
            client_attr.setName("CORTANA");
            client_attr.setVersion("3.0");
            bool wakeword_status;
            if (config->Read(&wakeword_status, voiceUIFramework::voiceUIUtils::Keys::k_cortana_wakeword_status_) {
                client_attr.setWakewordStatus(wakeword_status);
            }
            voiceui_clients.emplace_back(client_attr);
            enable_client = false;
        }
    }
    */
    // check modular client support
    if (config->Read(&enable_client, voiceUIFramework::voiceUIUtils::Keys::k_modular_status_)) {
        if (enable_client) {
            client_attr.setName("MODULAR");
            client_attr.setVersion("1.0");
            bool wakeword_status;
            if (config->Read(&wakeword_status, voiceUIFramework::voiceUIUtils::Keys::k_modular_wakeword_status_)) {
                client_attr.setWakewordStatus(wakeword_status);
            }
            voiceui_clients.emplace_back(client_attr);
            enable_client = false;
        }
    }
    return voiceui_clients;
}

OCEntityHandlerResult VoiceUISettingsHandler::postVoiceUISettings(VoiceUIServerAttr attr) {
    ADK_LOG_INFO("VoiceUISettingsHandler::postVoiceUISettings");
    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();

    if (unpacked.find(std::string(kDefaultVoiceUIClient)) != unpacked.end()) {
        std::string default_client{attr.getDefaultVoiceUIClient()};
        if (default_client.empty()) {
            ADK_LOG_ERROR("INVALID REQUEST");
            return OC_EH_BAD_REQ;
        }
        if (!adk_ipc_->setDefaultVoiceUIClient(default_client)) {
            return OC_EH_ERROR;
        }
    }

    if (unpacked.find(std::string(kAVSLocale)) != unpacked.end()) {
        std::string avs_locale = attr.getAVSLocale();
        if (avs_locale.empty()) {
            ADK_LOG_ERROR("INVALID REQUEST");
            return OC_EH_BAD_REQ;
        }
        if (!adk_ipc_->setAvsLocale(avs_locale)) {
            return OC_EH_ERROR;
        }
    }

    // TODO(Shravya): Enable when modular client languages is available
    /*
    if (unpacked.find(std::string(kSelectedModularClientLanguage)) != unpacked.end()) {
        std::string modular_client_language = attr.getSelectedModularClientLanguage();
        if (modular_client_language.empty()) {
            ADK_LOG_ERROR("INVALID REQUEST");
            return OC_EH_BAD_REQ;
        }
        if (!adk_ipc_->setModularClientLanguage(modular_client_language)) {
            return OC_EH_ERROR;
        }
    }
    */

    if (unpacked.find(std::string(kSelectedASRProvider)) != unpacked.end()) {
        std::string asr_provider = attr.getSelectedASRProvider();
        if (asr_provider.empty()) {
            ADK_LOG_ERROR("INVALID REQUEST");
            return OC_EH_BAD_REQ;
        }
        if (!adk_ipc_->setSelectedASRProvider(asr_provider)) {
            return OC_EH_ERROR;
        }
    }

    if (unpacked.find(std::string(kSelectedNLUProvider)) != unpacked.end()) {
        std::string nlu_provider = attr.getSelectedNLUProvider();
        if (nlu_provider.empty()) {
            ADK_LOG_ERROR("INVALID REQUEST");
            return OC_EH_BAD_REQ;
        }
        if (!adk_ipc_->setSelectedTTSProvider(nlu_provider)) {
            return OC_EH_ERROR;
        }
    }

    if (unpacked.find(std::string(kSelectedTTSProvider)) != unpacked.end()) {
        std::string tts_provider = attr.getSelectedTTSProvider();
        if (tts_provider.empty()) {
            ADK_LOG_ERROR("INVALID REQUEST");
            return OC_EH_BAD_REQ;
        }
        if (!adk_ipc_->setSelectedNLUProvider(tts_provider)) {
            return OC_EH_ERROR;
        }
    }

    return OC_EH_OK;
}

OCEntityHandlerResult VoiceUISettingsHandler::enableVoiceUIClient(SelectClientInServerAttr attr) {
    ADK_LOG_INFO("VoiceUISettingsHandler::enableVoiceUIClient");

    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();
    if ((unpacked.find(std::string(kClient)) == unpacked.end()) || (unpacked.find(std::string(kWakewordStatus)) == unpacked.end())) {
        ADK_LOG_ERROR("INVALID REQUEST");
        return OC_EH_BAD_REQ;
    }
    std::string client = attr.getClient();
    bool wakeword_status = attr.getWakewordStatus();
    if (client.empty()) {
        return OC_EH_BAD_REQ;
    }
    if (!adk_ipc_->enableVoiceUIClient(client, wakeword_status)) {
        return OC_EH_ERROR;
    }

    return OC_EH_OK;
}

OCEntityHandlerResult VoiceUISettingsHandler::startAVSOnboarding() {
    ADK_LOG_INFO("VoiceUISettingsHandler::startAVSOnboarding");

    // this code segmant handles case where onboarding is in progress
    // and valid URL and Code are stored in DB
    std::unique_ptr<adk::config::Config> config =
        adk::config::Config::Create(VOICEUI_CONFIG_FILENAME);
    if (config) {
        std::string url;
        std::string code;
        if (config->Read(&url,
                voiceUIFramework::voiceUIUtils::Keys::k_avs_authorization_url_)) {
            if (config->Read(&code,
                    voiceUIFramework::voiceUIUtils::Keys::k_avs_authorization_code_)) {
                if (!url.empty() && !code.empty()) {
                    ADK_LOG_INFO("VoiceUISettingsHandler::url %s code %s",
                        url.c_str(), code.c_str());
                    authenticateAvs(url, code);
                    return OC_EH_OK;
                } else {
                    ADK_LOG_INFO(
                        "VoiceUISettingsHandler:: valid url and code not available, starting onboarding");
                }
            }
        }
    }

    // we dont have valid URL and CODE
    // which means currently avs onboarding is not in progress, start onboarding
    if (!adk_ipc_->startOnboarding("AVS")) {
        return OC_EH_ERROR;
    }

    return OC_EH_OK;
}

OCEntityHandlerResult VoiceUISettingsHandler::deleteCredential(ClientInServerAttr attr) {
    ADK_LOG_INFO("VoiceUISettingsHandler::deleteCredential");
    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();
    if (unpacked.find(std::string(kClient)) == unpacked.end()) {
        ADK_LOG_ERROR("INVALID REQUEST");
        return OC_EH_BAD_REQ;
    }
    std::string client = attr.getClient();
    if (client.empty()) {
        return OC_EH_BAD_REQ;
    }
    if (!adk_ipc_->deleteVoiceUICredential(client)) {
        return OC_EH_ERROR;
    }

    return OC_EH_OK;
}

void VoiceUISettingsHandler::voiceUIDbUpdated() {
    ADK_LOG_INFO("VoiceUISettingsHandler::voiceUIDbUpdated");
    OCRepresentation rep;
    OCEntityHandlerResult result;
    std::tie(result, rep) = getVoiceUISettings();
    controller_->notifyVoiceUISettings(rep);
}

void VoiceUISettingsHandler::authenticateAvs(std::string url,
    std::string code) {
    ADK_LOG_INFO("VoiceUISettingsHandler::authenticateAvs");

    VoiceUIServerAttr vui_server_attr;
    AuthenticateAVSServerAttr attr;
    attr.setUrl(url);
    attr.setCode(code);
    vui_server_attr.setAuthenticateAVS(attr);
    controller_->notifyVoiceUISettings(vui_server_attr.pack());
}

void VoiceUISettingsHandler::avsOnboardingError(std::string client,
    std::string error,
    int reattempt_sec) {
    ADK_LOG_INFO("VoiceUISettingsHandler::avsOnboardingError");
    VoiceUIServerAttr vui_server_attr;
    AVSOnboardingErrorServerAttr attr;
    attr.setClient(client);
    attr.setError((error == "timedout")
            ? AVSOnboardingErrorServerAttr::Error::kTimedout
            : AVSOnboardingErrorServerAttr::Error::kUnknown);
    attr.setReattempt(reattempt_sec);
    vui_server_attr.setAVSOnboardingError(attr);
    controller_->notifyVoiceUISettings(vui_server_attr.pack());
}

}  // namespace iotivity_iotsys
}  // namespace adk
