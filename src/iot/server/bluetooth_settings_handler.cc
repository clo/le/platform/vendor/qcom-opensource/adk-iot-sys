/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "iot/server/bluetooth_settings_handler.h"

#include "adk/log.h"
#include "adk_ipc/adk_ipc.h"
#include "iot/server/controller.h"

namespace adk {
namespace iotivity_iotsys {

BluetoothSettingsHandler::BluetoothSettingsHandler(Controller* controller)
    : adk_ipc_(AdkIpc::getInstance()), controller_(controller) {
    ADK_LOG_INFO("BluetoothSettingsHandler::BluetoothSettingsHandler");
    adk_ipc_->registerBluetoothSettingsListener(this);
}

bool BluetoothSettingsHandler::getBTSettings() {
    ADK_LOG_INFO("BluetoothSettingsHandler::getBTSettings");

    if (!adk_ipc_->getBtAdapterState()) {
        return false;
    }

    if (!adk_ipc_->getPairedBtDevices()) {
        return false;
    }

    return true;
}

OCEntityHandlerResult BluetoothSettingsHandler::enable(EnableBtAdapterInServerAttr attr) {
    ADK_LOG_INFO("BluetoothSettingsHandler::enable");
    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();
    if (unpacked.find(std::string(kAdapterState)) == unpacked.end()) {
        ADK_LOG_ERROR("BluetoothSettingsHandler::enable adapter state attribute not found");
        return OC_EH_BAD_REQ;
    }
    if (!adk_ipc_->enableBt(attr.getAdapterState())) {
        return OC_EH_ERROR;
    }

    return OC_EH_OK;
}

OCEntityHandlerResult BluetoothSettingsHandler::startScan() {
    ADK_LOG_INFO("BluetoothSettingsHandler::startScan");
    if (!adk_ipc_->startBtScan()) {
        return OC_EH_ERROR;
    }
    return OC_EH_OK;
}

OCEntityHandlerResult BluetoothSettingsHandler::stopScan() {
    ADK_LOG_INFO("BluetoothSettingsHandler::stopScan");
    if (!adk_ipc_->stopBtScan()) {
        return OC_EH_ERROR;
    }
    return OC_EH_OK;
}

OCEntityHandlerResult BluetoothSettingsHandler::pair(AddressInServerAttr attr) {
    ADK_LOG_INFO("BluetoothSettingsHandler::pair");
    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();
    if (unpacked.find(std::string(kAddress)) == unpacked.end()) {
        ADK_LOG_ERROR("BluetoothSettingsHandler::pair address attribute not found");
        return OC_EH_BAD_REQ;
    }
    std::string address{attr.getAddress()};
    if (address.empty()) {
        ADK_LOG_ERROR("BluetoothSettingsHandler::pair Empty Address");
        return OC_EH_BAD_REQ;
    }
    if (!adk_ipc_->pairBtDevice(address)) {
        return OC_EH_ERROR;
    }

    return OC_EH_OK;
}

OCEntityHandlerResult BluetoothSettingsHandler::unpair(AddressInServerAttr attr) {
    ADK_LOG_INFO("BluetoothSettingsHandler::unpair");
    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();
    if (unpacked.find(std::string(kAddress)) == unpacked.end()) {
        ADK_LOG_ERROR("BluetoothSettingsHandler::unpair address attribute not found");
        return OC_EH_BAD_REQ;
    }
    std::string address{attr.getAddress()};
    if (address.empty()) {
        ADK_LOG_ERROR("BluetoothSettingsHandler::unpair Empty Address");
        return OC_EH_BAD_REQ;
    }
    if (!adk_ipc_->unpairBtDevice(address)) {
        return OC_EH_ERROR;
    }

    return OC_EH_OK;
}

OCEntityHandlerResult BluetoothSettingsHandler::connect(AddressInServerAttr attr) {
    ADK_LOG_INFO("BluetoothSettingsHandler::connect");
    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();
    if (unpacked.find(std::string(kAddress)) == unpacked.end()) {
        ADK_LOG_ERROR("BluetoothSettingsHandler::connect address attribute not found");
        return OC_EH_BAD_REQ;
    }
    std::string address{attr.getAddress()};
    if (address.empty()) {
        ADK_LOG_ERROR("BluetoothSettingsHandler::connect Empty Address");
        return OC_EH_BAD_REQ;
    }
    if (!adk_ipc_->connectBtDevice(address)) {
        return OC_EH_ERROR;
    }

    return OC_EH_OK;
}

OCEntityHandlerResult BluetoothSettingsHandler::disconnect(AddressInServerAttr attr) {
    ADK_LOG_INFO("BluetoothSettingsHandler::disconnect");
    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();
    if (unpacked.find(std::string(kAddress)) == unpacked.end()) {
        ADK_LOG_ERROR("BluetoothSettingsHandler::disconnect address attribute not found");
        return OC_EH_BAD_REQ;
    }
    std::string address{attr.getAddress()};
    if (address.empty()) {
        ADK_LOG_ERROR("BluetoothSettingsHandler::disconnect Empty Address");
        return OC_EH_BAD_REQ;
    }
    if (!adk_ipc_->disconnectBtDevice(address)) {
        return OC_EH_ERROR;
    }

    return OC_EH_OK;
}

OCEntityHandlerResult BluetoothSettingsHandler::clearPairedList() {
    ADK_LOG_INFO("BluetoothSettingsHandler::clearPairedList");
    if (!adk_ipc_->clearPairedBtDevices()) {
        return OC_EH_ERROR;
    }
    return OC_EH_OK;
}

OCEntityHandlerResult BluetoothSettingsHandler::getPairedDevices() {
    ADK_LOG_INFO("BluetoothSettingsHandler::getPairedDevices");
    if (!adk_ipc_->getPairedBtDevices()) {
        return OC_EH_ERROR;
    }
    return OC_EH_OK;
}

OCEntityHandlerResult BluetoothSettingsHandler::enterDiscoveryMode(TimeoutSecInServerAttr attr) {
    ADK_LOG_INFO("BluetoothSettingsHandler::enterDiscoveryMode");
    std::unordered_set<std::string> unpacked = attr.unpackedAttributes();
    if (unpacked.find(std::string(kTimeoutSecs)) == unpacked.end()) {
        ADK_LOG_ERROR("BluetoothSettingsHandler::enterDiscoveryMode timeout attribute not found");
        return OC_EH_BAD_REQ;
    }
    int timeout_secs = attr.getTimeoutSecs();
    if (timeout_secs < 0) {
        return OC_EH_BAD_REQ;
    }
    if (!adk_ipc_->enterBtDiscoveryMode(timeout_secs)) {
        return OC_EH_ERROR;
    }

    return OC_EH_OK;
}

OCEntityHandlerResult BluetoothSettingsHandler::getBtAdapterState() {
    ADK_LOG_INFO("BluetoothSettingsHandler::getBtAdapterState");
    if (!adk_ipc_->getBtAdapterState()) {
        return OC_EH_ERROR;
    }
    return OC_EH_OK;
}

OCEntityHandlerResult BluetoothSettingsHandler::cancelDiscoveryMode() {
    ADK_LOG_INFO("BluetoothSettingsHandler::cancelDiscoveryMode");
    if (!adk_ipc_->cancelBtDiscoveryMode()) {
        return OC_EH_ERROR;
    }
    return OC_EH_OK;
}

void BluetoothSettingsHandler::btAdapterState(bool enabled) {
    ADK_LOG_INFO("BluetoothSettingsHandler::btAdapterState : %d", enabled);

    BluetoothServerAttr attr;
    if (enabled) {
        attr.setAdapterState(BluetoothServerAttr::AdapterState::kEnabled);
    } else {
        attr.setAdapterState(BluetoothServerAttr::AdapterState::kDisabled);
    }
    controller_->notifyBluetoothSettings(attr.pack());
}

void BluetoothSettingsHandler::btDeviceDiscoverabilityStateChanged(
    bool discoverable) {
    ADK_LOG_INFO("BluetoothSettingsHandler::btDeviceDiscoverabilityStateChanged %d",
        discoverable);

    BluetoothServerAttr attr;
    if (discoverable) {
        attr.setDiscoverable(BluetoothServerAttr::Discoverable::kDiscoverable);
    } else {
        attr.setDiscoverable(
            BluetoothServerAttr::Discoverable::kNonDiscoverable);
    }
    controller_->notifyBluetoothSettings(attr.pack());
}

void BluetoothSettingsHandler::btDeviceConnectionStateChanged(
    const std::string& name, const std::string& address, bool connected) {
    ADK_LOG_INFO("BluetoothSettingsHandler::btDeviceConnectionStateChanged %s: %s: %d",
        name.c_str(), address.c_str(), connected);

    BluetoothServerAttr btser_attr;
    BtDeviceStatusServerAttr attr;
    attr.setName(name);
    attr.setAddress(address);
    attr.setStatus(connected);
    btser_attr.setConnectedState(attr);
    controller_->notifyBluetoothSettings(btser_attr.pack());
}

void BluetoothSettingsHandler::scanResult(const std::string& name, const std::string& address) {
    ADK_LOG_INFO("BluetoothSettingsHandler::scanResult %s: %s", name.c_str(), address.c_str());

    BluetoothServerAttr btser_attr;
    BtDeviceServerAttr attr;
    attr.setName(name);
    attr.setAddress(address);
    btser_attr.setScanResult(attr);
    controller_->notifyBluetoothSettings(btser_attr.pack());
}

void BluetoothSettingsHandler::pairedDevices(
    const std::vector<BtPairedDevice>& paired_devices) {
    ADK_LOG_INFO("BluetoothSettingsHandler::pairedDevices");

    BluetoothServerAttr attr;
    std::vector<BtDeviceStatusServerAttr> paired_bt_devices;

    for (auto& it : paired_devices) {
        BtDeviceStatusServerAttr attr2;

        if (it.received_address)
            attr2.setAddress(it.address);

        if (it.received_name)
            attr2.setName(it.name);

        if (it.received_connected)
            attr2.setStatus(it.connected);

        paired_bt_devices.emplace_back(std::move(attr2));
    }
    attr.setPairedDevices(paired_bt_devices);
    controller_->notifyBluetoothSettings(attr.pack());
}

void BluetoothSettingsHandler::btDevicePairStateChanged(const std::string& name,
    const std::string& address, bool paired) {
    ADK_LOG_INFO("BluetoothSettingsHandler::btDevicePairStateChanged %s: %s: %d",
        name.c_str(), address.c_str(), paired);

    BluetoothServerAttr btser_attr;
    BtDeviceStatusServerAttr attr;
    attr.setName(name);
    attr.setAddress(address);
    attr.setStatus(paired);
    btser_attr.setPairedState(attr);
    controller_->notifyBluetoothSettings(btser_attr.pack());
}

void BluetoothSettingsHandler::btScanStopped(const std::string& status) {
    ADK_LOG_INFO("BluetoothSettingsHandler::btScanStopped status:%s", status.c_str());

    BluetoothServerAttr btser_attr;
    BtSignalServerAttr attr;
    attr.setName(BtSignalServerAttr::Name::kScanStopped);
    attr.setStatus(status);
    btser_attr.setSignal(attr);
    controller_->notifyBluetoothSettings(btser_attr.pack());
}

void BluetoothSettingsHandler::btPairedListCleared(const std::string& status) {
    ADK_LOG_INFO("BluetoothSettingsHandler::btPairedListCleared %s", status.c_str());

    BluetoothServerAttr btser_attr;
    BtSignalServerAttr attr;
    attr.setName(BtSignalServerAttr::Name::kPairedListCleared);
    attr.setStatus(status);
    btser_attr.setSignal(attr);
    controller_->notifyBluetoothSettings(btser_attr.pack());
}

void BluetoothSettingsHandler::btError(const std::string& address, const std::string& status) {
    ADK_LOG_INFO("BluetoothSettingsHandler::btError addr:%s status:%s",
        address.c_str(), status.c_str());

    BluetoothServerAttr btser_attr;
    BtErrorServerAttr attr;
    attr.setAddress(address);
    attr.setStatus(status);
    btser_attr.setError(attr);
    controller_->notifyBluetoothSettings(btser_attr.pack());
}

}  // namespace iotivity_iotsys
}  // namespace adk
