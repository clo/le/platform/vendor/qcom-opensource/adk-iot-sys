/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "adk_ipc/adk_ipc.h"

#include <tuple>

#include "adk/log.h"

namespace adk {
namespace iotivity_iotsys {

static const char kIPCName[] = "iotsys";

AdkIpc::AdkIpc()
    : message_service_(kIPCName) {
}

AdkIpc* AdkIpc::instance_ = nullptr;

AdkIpc* AdkIpc::getInstance() {
    if (instance_ == nullptr) {
        instance_ = new AdkIpc();
    }
    return instance_;
}

bool AdkIpc::Start() {
    if (!message_service_.Initialise()) {
        return false;
    }

    using std::placeholders::_1;
    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityNetworkInfo,
        std::bind(&AdkIpc::networkStateInfo, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityBtEnabled,
        std::bind(&AdkIpc::btAdapterEnabled, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityBtDisabled,
        std::bind(&AdkIpc::btAdapterDisabled, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityBtConnected,
        std::bind(&AdkIpc::btDeviceConnected, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityBtDisconnected,
        std::bind(&AdkIpc::btDeviceDisconnected, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityBtScanresult,
        std::bind(&AdkIpc::btScanResult, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityBtScanstopped,
        std::bind(&AdkIpc::btScanStopped, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityBtErrorresponse,
        std::bind(&AdkIpc::btErrorResponse, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityBtPaired,
        std::bind(&AdkIpc::btDevicePaired, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityBtUnpaired,
        std::bind(&AdkIpc::btDeviceUnpaired, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityBtClearpairedlist,
        std::bind(&AdkIpc::btPairedListCleared, this, _1));

    message_service_.Subscribe(adk::msg::AdkMessage::kConnectivityBtPairlist,
        std::bind(&AdkIpc::btGetPairedDevicesResult, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityBtDiscoverable,
        std::bind(&AdkIpc::btDiscoverable, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityBtState,
        std::bind(&AdkIpc::btAdapterState, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kVoiceuiDatabaseUpdated,
        std::bind(&AdkIpc::voiceUIDbUpdated, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kVoiceuiAuthenticateAvs,
        std::bind(&AdkIpc::authenticateAvs, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kVoiceuiAvsOnboardingError,
        std::bind(&AdkIpc::avsOnboardingError, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityZigbeeCoordinatorStatus,
        std::bind(&AdkIpc::zbCoordinatorState, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityZigbeeDeviceList,
        std::bind(&AdkIpc::zbJoinedDevices, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityZigbeeDisabled,
        std::bind(&AdkIpc::zbAdapterDisabled, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityZigbeeEnabled,
        std::bind(&AdkIpc::zbAdapterEnabled, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityZigbeeError,
        std::bind(&AdkIpc::zbError, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityZigbeeNetworkFormed,
        std::bind(&AdkIpc::zbNetworkFormed, this, _1));

    message_service_.Subscribe(
        adk::msg::AdkMessage::kConnectivityZigbeeNetworkJoined,
        std::bind(&AdkIpc::zbNetworkJoined, this, _1));
    return true;
}

void AdkIpc::registerSystemSettingsListener(SystemSettingsListener* listener) {
    system_settings_listener_ = listener;
}

void AdkIpc::registerNetworkSettingsListener(
    NetworkSettingsListener* listener) {
    network_settings_listener_ = listener;
}

void AdkIpc::registerBluetoothSettingsListener(
    BluetoothSettingsListener* listener) {
    bt_settings_listener_ = listener;
}

void AdkIpc::registerVoiceUISettingsListener(
    VoiceUISettingsListener* listener) {
    voiceui_settings_listener_ = listener;
}

void AdkIpc::registerZigbeeSettingsListener(
    ZigbeeSettingsListener* listener) {
    zigbee_settings_listener_ = listener;
}

bool AdkIpc::deviceFriendlyNameChanged(
    const std::string& device_friendly_name) {
    ADK_LOG_INFO("AdkIpc::deviceFriendlyNameChanged");

    adk::msg::AdkMessage message;
    auto name_changed_message =
        message.mutable_iotsys_device_friendly_name_updated();
    name_changed_message->set_name(device_friendly_name);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::deviceFriendlyNameChanged failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::enableVoiceUI(bool enable) {
    ADK_LOG_INFO("AdkIpc::enableVoiceUI");
    adk::msg::AdkMessage message;
    auto status_message = message.mutable_voiceui_status();
    status_message->set_client("ALL");
    status_message->set_status(enable);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::enableVoiceUI failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::enableVoiceUIClient(const std::string& client, bool wakeword_status) {
    ADK_LOG_INFO("AdkIpc::enableVoiceUIClient");
    adk::msg::AdkMessage message;
    auto status_message = message.mutable_voiceui_status();
    status_message->set_client(client);
    status_message->set_status(wakeword_status);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::enableVoiceUIClient failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::setDefaultVoiceUIClient(const std::string& client) {
    ADK_LOG_INFO("AdkIpc::setDefaultVoiceUIClient");
    adk::msg::AdkMessage message;
    auto default_client_message = message.mutable_voiceui_set_default_client();
    default_client_message->set_client(client);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::setDefaultVoiceUIClient failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::setAvsLocale(const std::string& locale) {
    ADK_LOG_INFO("AdkIpc::setAvsLocale");
    adk::msg::AdkMessage message;
    auto avs_locale_message = message.mutable_voiceui_set_avs_locale();
    avs_locale_message->set_locale(locale);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::setAvsLocale failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::startOnboarding(const std::string& client) {
    ADK_LOG_INFO("AdkIpc::startOnboarding");
    adk::msg::AdkMessage message;
    auto start_onboarding_message = message.mutable_voiceui_start_onboarding();
    start_onboarding_message->set_client(client);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::startOnboarding failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::deleteVoiceUICredential(const std::string& client) {
    ADK_LOG_INFO("AdkIpc::deleteVoiceUICredential");
    adk::msg::AdkMessage message;
    auto delete_credential_message = message.mutable_voiceui_delete_credential();
    delete_credential_message->set_client(client);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::deleteVoiceUICredential failed to send ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::setModularClientLanguage(const std::string& lang) {
    ADK_LOG_INFO("AdkIpc::setModularClientLanguage");
    adk::msg::AdkMessage message;
    auto modular_client_props_message =
        message.mutable_voiceui_set_modular_client_props();
    modular_client_props_message->set_language(lang);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::setModularClientLanguage failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::setSelectedASRProvider(const std::string& asr_provider) {
    ADK_LOG_INFO("AdkIpc::setSelectedASRProvider");
    adk::msg::AdkMessage message;
    auto modular_client_props_message =
        message.mutable_voiceui_set_modular_client_props();
    modular_client_props_message->set_asr(asr_provider);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::setSelectedASRProvider failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::setSelectedTTSProvider(const std::string& tts_provider) {
    ADK_LOG_INFO("AdkIpc::setSelectedTTSProvider");
    adk::msg::AdkMessage message;
    auto modular_client_props_message =
        message.mutable_voiceui_set_modular_client_props();
    modular_client_props_message->set_tts(tts_provider);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::setSelectedTTSProvider failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::setSelectedNLUProvider(const std::string& nlu_provider) {
    ADK_LOG_INFO("AdkIpc::setSelectedNLUProvider");
    adk::msg::AdkMessage message;
    auto modular_client_props_message =
        message.mutable_voiceui_set_modular_client_props();
    modular_client_props_message->set_nlu(nlu_provider);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::setSelectedNLUProvider failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::getNetworkStateInfo() {
    ADK_LOG_INFO("AdkIpc::getNetworkStateInfo");
    adk::msg::AdkMessage message;
    message.mutable_connectivity_get_network_info();
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::getNetworkStateInfo failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::enableBt(bool enable) {
    ADK_LOG_INFO("AdkIpc::enableBt");
    adk::msg::AdkMessage message;
    if (enable) {
        message.mutable_connectivity_bt_enable();
    } else {
        message.mutable_connectivity_bt_disable();
    }
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::enableBt failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::startBtScan() {
    ADK_LOG_INFO("AdkIpc::startBtScan");
    adk::msg::AdkMessage message;
    message.mutable_connectivity_bt_scanstart();
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::startBtScan failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::stopBtScan() {
    ADK_LOG_INFO("AdkIpc::stopBtScan");
    adk::msg::AdkMessage message;
    message.mutable_connectivity_bt_scanstop();
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::stopBtScan failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::pairBtDevice(const std::string& address) {
    ADK_LOG_INFO("AdkIpc::pairBtDevice");
    adk::msg::AdkMessage message;
    auto pair_device_message = message.mutable_connectivity_bt_pair();
    pair_device_message->set_address(address);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::pairBtDevice failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::unpairBtDevice(const std::string& address) {
    ADK_LOG_INFO("AdkIpc::unpairBtDevice");
    adk::msg::AdkMessage message;
    auto pair_device_message = message.mutable_connectivity_bt_unpair();
    pair_device_message->set_address(address);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::unpairBtDevice failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::getPairedBtDevices() {
    ADK_LOG_INFO("AdkIpc::getPairedBtDevices");
    adk::msg::AdkMessage message;
    message.mutable_connectivity_bt_getpairedlist();
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::getPairedBtDevices failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::connectBtDevice(const std::string& address) {
    ADK_LOG_INFO("AdkIpc::connectBtDevice");
    adk::msg::AdkMessage message;
    auto connect_message = message.mutable_connectivity_bt_connect();
    connect_message->set_address(address);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::connectBtDevice failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::disconnectBtDevice(const std::string& address) {
    ADK_LOG_INFO("AdkIpc::disconnectBtDevice");
    adk::msg::AdkMessage message;
    auto disconnect_message = message.mutable_connectivity_bt_disconnect();
    disconnect_message->set_address(address);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::disconnectBtDevice failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::clearPairedBtDevices() {
    ADK_LOG_INFO("AdkIpc::clearPairedBtDevices");
    adk::msg::AdkMessage message;
    message.mutable_connectivity_bt_clearpairedlist();
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::clearPairedBtDevices failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::enterBtDiscoveryMode(int timeout_secs) {
    ADK_LOG_INFO("AdkIpc::enterBtDiscoveryMode");
    adk::msg::AdkMessage message;
    auto discoverable_message = message.mutable_connectivity_bt_setdiscoverable();
    discoverable_message->set_timeout(timeout_secs);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::enterBtDiscoveryMode failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::getBtAdapterState() {
    ADK_LOG_INFO("AdkIpc::getBtAdapterState");
    adk::msg::AdkMessage message;
    message.mutable_connectivity_bt_getstate();
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::getBtAdapterState failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::cancelBtDiscoveryMode() {
    ADK_LOG_INFO("AdkIpc::cancelBtDiscoveryMode");
    adk::msg::AdkMessage message;
    message.mutable_connectivity_bt_exitdiscoverable();
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::cancelBtDiscoveryMode failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::setBTDeviceName(const std::string& device_friendly_name) {
    adk::msg::AdkMessage message;
    auto bt_message =
        message.mutable_connectivity_bt_setname();
    bt_message->set_name(device_friendly_name);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::setBTDeviceName failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::enableZb(bool enable) {
    ADK_LOG_INFO("AdkIpc::enableZb");
    adk::msg::AdkMessage message;
    if (enable) {
        message.mutable_connectivity_zigbee_enable();
    } else {
        message.mutable_connectivity_zigbee_disable();
    }
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::enableZb failed to send the ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::formZbNetwork() {
    ADK_LOG_INFO("AdkIpc::formZbNetwork");
    adk::msg::AdkMessage message;
    message.mutable_connectivity_zigbee_form_network();
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::formZbNetwork Failed to send ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::joinZbNetwork() {
    ADK_LOG_INFO("AdkIpc::joinZbNetwork");
    adk::msg::AdkMessage message;
    message.mutable_connectivity_zigbee_join_network();
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::joinZbNetwork Failed to send ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::setZbFriendlyName(int64_t device_identifier, const std::string& friendly_name) {
    ADK_LOG_INFO("AdkIpc::setZbFriendlyName");
    adk::msg::AdkMessage message;
    auto friendly_name_message = message.mutable_connectivity_zigbee_set_device_name();
    friendly_name_message->set_device_identifier(device_identifier);
    friendly_name_message->set_name(friendly_name);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::setZbFriendlyName Failed to send ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::startZbJoining(int timeout_secs) {
    ADK_LOG_INFO("AdkIpc::startZbJoining");
    adk::msg::AdkMessage message;
    auto allow_joining_message = message.mutable_connectivity_zigbee_allow_device_joining();
    allow_joining_message->set_duration(timeout_secs);
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::startZbJoining Failed to send ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::getZbAdapterStatus() {
    ADK_LOG_INFO("AdkIpc::getZbAdapterStatus");
    adk::msg::AdkMessage message;
    message.mutable_connectivity_zigbee_get_adapter_state();
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::getZbAdapterStatus Failed to send ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::getZbCoordinatorStatus() {
    ADK_LOG_INFO("AdkIpc::getZbCoordinatorStatus");
    adk::msg::AdkMessage message;
    message.mutable_connectivity_zigbee_get_coordinator_status();
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::getZbCoordinatorStatus Failed to send ipc signal");
        return false;
    }
    return true;
}

bool AdkIpc::getZbDeviceList() {
    ADK_LOG_INFO("AdkIpc::getZbDeviceList");
    adk::msg::AdkMessage message;
    message.mutable_connectivity_zigbee_request_device_list();
    if (!message_service_.Send(message)) {
        ADK_LOG_ERROR("AdkIpc::getZbDeviceList Failed to send ipc signal");
        return false;
    }
    return true;
}

void AdkIpc::networkStateInfo(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};

    ADK_LOG_INFO("AdkIpc::networkStateInfo namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());
    if (network_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::networkStateInfo no registered listener found");
        return;
    }

    NetworkInfoReceived info;

    if (message.connectivity_network_info().has_home_connection()) {

        //Fill the fields related to "connectivity" in the "info" structure
        info.received_connectivity =
            message.connectivity_network_info().home_connection().has_connectivity();
        if (info.received_connectivity) {
            info.connectivity =
                message.connectivity_network_info().home_connection().connectivity();
        }

        //Fill the fields related to "ssid" in the "info" structure
        info.received_ssid =
            message.connectivity_network_info().home_connection().has_ssid();
        if (info.received_ssid) {
            info.ssid = message.connectivity_network_info().home_connection().ssid();
        }

        //Fill the fields related to "rssi" in the "info" structure
        info.received_rssi =
            message.connectivity_network_info().home_connection().has_rssi();
        if (info.received_rssi) {
            info.rssi = message.connectivity_network_info().home_connection().rssi();
        }

        //Fill the fields related to "ip_address" in the "info" structure
        info.received_ip_address =
            message.connectivity_network_info().home_connection().has_ip_address();
        if (info.received_ip_address) {
            info.ip_address = message.connectivity_network_info().home_connection().ip_address();
        }

        //Fill the fields related to "mac_address" in the "info" structure
        info.received_mac_address =
            message.connectivity_network_info().home_connection().has_mac_address();
        if (info.received_mac_address) {
            info.mac_address = message.connectivity_network_info().home_connection().mac_address();
        }

        network_settings_listener_->networkStateInfo(info);
    }
    else
        ADK_LOG_INFO("AdkIpc::networkStateInfo NetworkInfo has no home_connection");
}

void AdkIpc::btAdapterEnabled(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btAdapterEnabled namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());
    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btAdapterEnabled no registered listener found");
        return;
    }
    if (message.connectivity_bt_enabled().status() == "success") {
        bt_settings_listener_->btAdapterState(true);
    } else {
        ADK_LOG_ERROR("AdkIpc::btAdapterEnabled enable failed");
    }
}

void AdkIpc::btAdapterDisabled(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btAdapterDisabled namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());
    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btAdapterDisabled no registered listener found");
        return;
    }
    if (message.connectivity_bt_disabled().status() == "success") {
        bt_settings_listener_->btAdapterState(false);
    } else {
        ADK_LOG_ERROR("AdkIpc::btAdapterDisabled disable failed");
    }
}

void AdkIpc::btDeviceConnected(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btDeviceConnected namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btDeviceConnected no registered listener found");
        return;
    }
    std::string address{message.connectivity_bt_connected().address()};
    // bt connected response does not contain a name field.
    bt_settings_listener_->btDeviceConnectionStateChanged({}, address, true);
}

void AdkIpc::btDeviceDisconnected(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btDeviceDisconnected namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());
    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btDeviceDisconnected no registered listener found");
        return;
    }
    std::string address{message.connectivity_bt_disconnected().address()};
    // bt disconnected response does not contain a name field.
    bt_settings_listener_->btDeviceConnectionStateChanged({}, address, false);
}

void AdkIpc::btScanResult(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btScanResult namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());
    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btScanResult no registered listener found");
        return;
    }
    std::string address{message.connectivity_bt_scanresult().address()};
    std::string name{message.connectivity_bt_scanresult().name()};
    bt_settings_listener_->scanResult(name, address);
}

void AdkIpc::btErrorResponse(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btErrorResponse namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());
    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btErrorResponse no registered listener found");
        return;
    }
    std::string address{message.connectivity_bt_errorresponse().address()};
    std::string status{message.connectivity_bt_errorresponse().status()};
    bt_settings_listener_->btError(address, status);
}

void AdkIpc::btScanStopped(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btScanStopped namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());
    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btScanStopped no registered listener found");
        return;
    }
    bt_settings_listener_->btScanStopped(
        message.connectivity_bt_scanstopped().status());
}

void AdkIpc::btDevicePaired(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btDevicePaired namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btDevicePaired no registered listener found");
        return;
    }
    std::string address{message.connectivity_bt_paired().address()};
    std::string name{message.connectivity_bt_paired().name()};
    bt_settings_listener_->btDevicePairStateChanged(name, address, true);
}

void AdkIpc::btDeviceUnpaired(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btDeviceUnpaired namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btDeviceUnpaired no registered listener found");
        return;
    }
    std::string address{message.connectivity_bt_unpaired().address()};
    std::string name{message.connectivity_bt_unpaired().name()};
    bt_settings_listener_->btDevicePairStateChanged(name, address, false);
}

void AdkIpc::btPairedListCleared(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btPairedListCleared namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btPairedListCleared no registered listener found");
        return;
    }
    std::string status{message.connectivity_bt_clearpairedliststatus().status()};
    bt_settings_listener_->btPairedListCleared(status);
}

void AdkIpc::btGetPairedDevicesResult(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btGetPairedDevicesResult namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btGetPairedDevicesResult no registered listener found");
        return;
    }
    std::vector<BtPairedDevice> paired_list;
    for (const auto& paired_device :
        message.connectivity_bt_pairlist().bt_pair_list()) {
        BtPairedDevice bt_paired_device;

        //Fill the fields related to "address" in the "bt_paired_device" structure
        bt_paired_device.received_address = paired_device.has_address();
        if (bt_paired_device.received_address)
            bt_paired_device.address = paired_device.address();

        //Fill the fields related to "name" in the "bt_paired_device" structure
        bt_paired_device.received_name = paired_device.has_name();
        if (bt_paired_device.received_name)
            bt_paired_device.name = paired_device.name();

        //Fill the fields related to "connected" in the "bt_paired_device" structure
        bt_paired_device.received_connected = paired_device.connected();
        if (bt_paired_device.received_connected)
            bt_paired_device.connected = paired_device.connected();

        paired_list.emplace_back(bt_paired_device);
    }
    bt_settings_listener_->pairedDevices(paired_list);
}

void AdkIpc::btDiscoverable(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btDiscoverable namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());
    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btDiscoverable no registered listener found");
        return;
    }
    if (message.connectivity_bt_discoverable().status() == "true") {
        bt_settings_listener_->btDeviceDiscoverabilityStateChanged(true);
        ADK_LOG_INFO("AdkIpc::btDiscoverable true");
    } else if (message.connectivity_bt_discoverable().status() == "false") {
        bt_settings_listener_->btDeviceDiscoverabilityStateChanged(false);
        ADK_LOG_INFO("AdkIpc::btDiscoverable false");
    } else {
        ADK_LOG_ERROR("AdkIpc::btDiscoverable invalid state received: %s",
            message.connectivity_bt_discoverable().status().c_str());
    }
}

void AdkIpc::btAdapterState(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::btAdapterState namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());
    if (bt_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::btAdapterState no registered listener found");
        return;
    }
    if (message.connectivity_bt_state().status() == "enabled") {
        bt_settings_listener_->btAdapterState(true);
        ADK_LOG_INFO("AdkIpc::btAdapterState enabled");
    } else if (message.connectivity_bt_state().status() == "disabled") {
        bt_settings_listener_->btAdapterState(false);
        ADK_LOG_INFO("AdkIpc::btAdapterState disabled");
    } else if (message.connectivity_bt_state().has_discoverable()) {
        if (message.connectivity_bt_state().discoverable() == true){
            bt_settings_listener_->btDeviceDiscoverabilityStateChanged(true);
            ADK_LOG_INFO("AdkIpc::btAdapterState discoverable true");
        } else if (message.connectivity_bt_state().discoverable() == false) {
            bt_settings_listener_->btDeviceDiscoverabilityStateChanged(false);
            ADK_LOG_INFO("AdkIpc::btAdapterState discoverable false");
        }
    } else {
        ADK_LOG_ERROR("AdkIpc::btAdapterState invalid state received: %s",
            message.connectivity_bt_state().status().c_str());
    }
}

void AdkIpc::voiceUIDbUpdated(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::voiceUIDbUpdated namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());
    if (voiceui_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::voiceUIDbUpdated no registered listener found");
        return;
    }
    voiceui_settings_listener_->voiceUIDbUpdated();
}

void AdkIpc::authenticateAvs(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::authenticateAvs namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (voiceui_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::authenticateAvs no registered listener found");
        return;
    }
    voiceui_settings_listener_->authenticateAvs(message.voiceui_authenticate_avs().url(),
        message.voiceui_authenticate_avs().code());
}

void AdkIpc::avsOnboardingError(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::avsOnboardingError namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (voiceui_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::avsOnboardingError no registered listener found");
        return;
    }
    std::string client, error;
    int reattempt = 0;  // time in seconds
    client = message.voiceui_avs_onboarding_error().client();
    error = message.voiceui_avs_onboarding_error().error();
    reattempt = message.voiceui_avs_onboarding_error().reattempt();
    voiceui_settings_listener_->avsOnboardingError(client, error, reattempt);
}

void AdkIpc::zbAdapterEnabled(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::zbAdapterEnabled namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (zigbee_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::zbAdapterEnabled no registered listener found");
        return;
    }
    if (message.connectivity_zigbee_enabled().status()) {
        zigbee_settings_listener_->zbAdapterState(true);
    } else {
        ADK_LOG_ERROR("AdkIpc::zbAdapterEnabled enable failed");
    }
}

void AdkIpc::zbAdapterDisabled(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::zbAdapterEnabled namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (zigbee_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::zbAdapterEnabled no registered listener found");
        return;
    }
    if (message.connectivity_zigbee_disabled().status()) {
        zigbee_settings_listener_->zbAdapterState(false);
    } else {
        ADK_LOG_ERROR("AdkIpc::zbAdapterDisabled disable failed");
    }
}

void AdkIpc::zbCoordinatorState(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::zbCoordinatorState namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (zigbee_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::zbCoordinatorState no registered listener found");
        return;
    }
    if (message.connectivity_zigbee_coordinator_status().status() == "true") {
        zigbee_settings_listener_->zbCoordinatorState(true);
    } else {
        zigbee_settings_listener_->zbCoordinatorState(false);
    }
}

void AdkIpc::zbError(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::zbError namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (zigbee_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::zbError no registered listener found");
        return;
    }
    zigbee_settings_listener_->zbError(message.connectivity_zigbee_error().status());
}

void AdkIpc::zbJoinedDevices(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::zbJoinedDevices namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (zigbee_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::zbJoinedDevices no registered listener found");
        return;
    }
    std::vector<ZigbeeDevice> joined_devices;
    for (const auto& joined_device :
        message.connectivity_zigbee_device_list().device_list()) {
        joined_devices.emplace_back(ZigbeeDevice{joined_device.name(),
            static_cast<int>(joined_device.device_identifier()),
            joined_device.device_type()});
    }
    zigbee_settings_listener_->zbJoinedDevices(joined_devices);
}

void AdkIpc::zbNetworkFormed(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::zbNetworkFormed namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (zigbee_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::zbNetworkFormed no registered listener found");
        return;
    }
    zigbee_settings_listener_->zbNetworkFormed(message.connectivity_zigbee_network_formed().status());
}

void AdkIpc::zbNetworkJoined(adk::msg::AdkMessage message) {
    auto dbus_args = adk::msg::oneof_id_to_dbus_args.at(message.adk_msg_case());
    std::string namespace_rcd{dbus_args.group};
    std::string property_rcd{dbus_args.name};
    ADK_LOG_INFO("AdkIpc::zbNetworkJoined namespace: %s property: %s",
        namespace_rcd.c_str(), property_rcd.c_str());

    if (zigbee_settings_listener_ == nullptr) {
        ADK_LOG_ERROR("AdkIpc::zbNetworkJoined no registered listener found");
        return;
    }
    zigbee_settings_listener_->zbNetworkJoined(message.connectivity_zigbee_network_joined().status());
}
}  // namespace iotivity_iotsys
}  // namespace adk
