/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SRC_ADK_IPC_ADK_IPC_H_
#define SRC_ADK_IPC_ADK_IPC_H_

#include <memory>

#include <adk/message-service/adk-message-service.h>

#include "iot/server/bluetooth_settings_listener.h"
#include "iot/server/controller.h"
#include "iot/server/network_settings_listener.h"
#include "iot/server/system_settings_listener.h"
#include "iot/server/voiceui_settings_listener.h"
#include "iot/server/zigbee_settings_listener.h"

namespace adk {
namespace iotivity_iotsys {

class AdkIpc {
 public:
    AdkIpc(const AdkIpc&) = delete;
    AdkIpc& operator=(const AdkIpc&) = delete;

    static AdkIpc* getInstance();

    bool Start();

    void registerSystemSettingsListener(SystemSettingsListener* listener);
    void registerNetworkSettingsListener(NetworkSettingsListener* listener);
    void registerBluetoothSettingsListener(BluetoothSettingsListener* listener);
    void registerVoiceUISettingsListener(VoiceUISettingsListener* listener);
    void registerZigbeeSettingsListener(ZigbeeSettingsListener* listener);

    // System settings
    bool deviceFriendlyNameChanged(const std::string& device_friendly_name);

    // VoiceUI settings
    bool enableVoiceUI(bool enable);
    bool setDefaultVoiceUIClient(const std::string& client);
    bool setAvsLocale(const std::string& locale);

    bool setModularClientAlertFile(const std::string& path);

    bool setModularClientLanguage(const std::string& lang);
    bool setSelectedASRProvider(const std::string& asr_provider);
    bool setSelectedTTSProvider(const std::string& tts_provider);
    bool setSelectedNLUProvider(const std::string& nlu_provider);

    bool enableVoiceUIClient(const std::string& client, bool wakeword_status);
    bool startOnboarding(const std::string& client);
    bool deleteVoiceUICredential(const std::string& client);

    // Network state
    bool getNetworkStateInfo();

    // Bluetooth settings
    bool enableBt(bool enable);
    bool startBtScan();
    bool stopBtScan();
    bool pairBtDevice(const std::string& address);
    bool unpairBtDevice(const std::string& address);
    bool getPairedBtDevices();
    bool connectBtDevice(const std::string& address);
    bool disconnectBtDevice(const std::string& address);
    bool clearPairedBtDevices();
    bool enterBtDiscoveryMode(int timeout_secs);
    bool getBtAdapterState();
    bool cancelBtDiscoveryMode();
    bool setBTDeviceName(const std::string& device_friendly_name);

    // Zigbee settings
    bool enableZb(bool enable);
    bool formZbNetwork();
    bool joinZbNetwork();
    bool setZbFriendlyName(int64_t device_identifier, const std::string& friendly_name);
    bool startZbJoining(int timeout_secs);
    bool getZbAdapterStatus();
    bool getZbCoordinatorStatus();
    bool getZbDeviceList();

 private:
    AdkIpc();

    // ipc signal listeners, each property has a listener
    void networkStateInfo(adk::msg::AdkMessage message);

    void btAdapterEnabled(adk::msg::AdkMessage message);
    void btAdapterDisabled(adk::msg::AdkMessage message);
    void btDeviceConnected(adk::msg::AdkMessage message);
    void btDeviceDisconnected(adk::msg::AdkMessage message);
    void btScanResult(adk::msg::AdkMessage message);
    void btScanStopped(adk::msg::AdkMessage message);
    void btErrorResponse(adk::msg::AdkMessage message);
    void btDevicePaired(adk::msg::AdkMessage message);
    void btDeviceUnpaired(adk::msg::AdkMessage message);
    void btPairedListCleared(adk::msg::AdkMessage message);
    void btGetPairedDevicesResult(adk::msg::AdkMessage message);
    void btDiscoverable(adk::msg::AdkMessage message);
    void btAdapterState(adk::msg::AdkMessage message);

    void voiceUIDbUpdated(adk::msg::AdkMessage message);
    void avsOnboardingError(adk::msg::AdkMessage message);
    void authenticateAvs(adk::msg::AdkMessage message);

    void zbAdapterEnabled(adk::msg::AdkMessage message);
    void zbAdapterDisabled(adk::msg::AdkMessage message);
    void zbCoordinatorState(adk::msg::AdkMessage message);
    void zbJoiningState(adk::msg::AdkMessage message);
    void zbJoinedDevices(adk::msg::AdkMessage message);
    void zbNetworkFormed(adk::msg::AdkMessage message);
    void zbNetworkJoined(adk::msg::AdkMessage message);
    void zbError(adk::msg::AdkMessage message);

 private:
    static AdkIpc* instance_;
    // listeners
    BluetoothSettingsListener* bt_settings_listener_ = nullptr;
    NetworkSettingsListener* network_settings_listener_ = nullptr;
    VoiceUISettingsListener* voiceui_settings_listener_ = nullptr;
    SystemSettingsListener* system_settings_listener_ = nullptr;
    ZigbeeSettingsListener* zigbee_settings_listener_ = nullptr;

    adk::msg::AdkMessageService message_service_;
};
}  // namespace iotivity_iotsys
}  // namespace adk
#endif  // SRC_ADK_IPC_ADK_IPC_H_
